package sistema;

import codigo.Mesa;
import guis.VentanaInicio;
import java.awt.*;
import javax.swing.JOptionPane;


public class Ejecutable {
    
    public static void main( String args[] ){
        VentanaInicio vi = new VentanaInicio();
        
        // Get the size of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        
        // Determine the new location of the window
        int w = vi.getSize().width;
        int h = vi.getSize().height;
        int x = (dim.width-w)/2;
        int y = (dim.height-h)/2;
        
        // Move the window
        vi.setLocation(x, y);
        
        vi.setVisible( true );
        Mesa.instanciar();
    }   //findel m�todo main
    
}   //fin de la clase Ejecutable