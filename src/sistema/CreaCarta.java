/*
 * CreaCarta.java
 *
 * Created on 19 de marzo de 2006, 11:15 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package sistema;

import codigo.Mesa;
import codigo.cartas.*;
import java.util.ArrayList;

/**
 *
 * @author __USER__
 */
public class CreaCarta {
    
    /** Creates a new instance of CreaCarta */
    public CreaCarta() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ArrayList cartas = new ArrayList();
        cartas.add(new CartaCarcel("El Senado lo ha convocado al Coliseum para que participe de los juegos"));
        cartas.add(new CartaCarcel("Ha sido capturado y convertido en un gladiator, debe asistir a los juegos"));
        cartas.add(new CartaAnularMov("El Senado ha anulado su movimiento porque no es del interes de Roma que usted se encuentre en esa Regi�n:\n" +
                "Debe regresar a la casilla anterior."));
        cartas.add(new CartaAnularMov("El Senado ha anulado su movimiento porque no es del interes de Roma que usted se encuentre en esa Regi�n:\n" +
                "Debe regresar a la casilla anterior."));
        cartas.add(new CartaAnularMov("El Senado ha anulado su movimiento porque no es del interes de Roma que usted se encuentre en esa Regi�n:\n" +
                "Debe regresar a la casilla anterior."));
        cartas.add(new CartaAnularMov("El Senado ha anulado su movimiento porque no es del interes de Roma que usted se encuentre en esa Regi�n:\n" +
                "Debe regresar a la casilla anterior."));
        cartas.add(new CartaCastigoSoldados("Su ejercito ha estado pasando hambre durante mucho tiempo\n" +
                "y se han muerto ", true, 2));
        cartas.add(new CartaCastigoSoldados("Su ejercito contrajo una efermedad muy contagiosa\n" +
                "y se han muerto ", true, 3));
        cartas.add(new CartaCastigoSoldados("Ha sucedido una inundacion en las barracas de la reserva. Su\n" +
                "reserva se ha perdido ", false, 3));
        cartas.add(new CartaCastigoSoldados("Algunos soldados de su reserva han desertado por no haber\n" +
                "sido llamado a combate, usted perdi� ", false, 2));
        cartas.add(new CartaPremioSoldados("El Senado le ha concedio el control sobre ", 1.5));
        cartas.add(new CartaPremioSoldados("Una tribu se ha puesto a sus ordenes y le dieron ", 0.5));
        cartas.add(new CartaPremioSoldados("Sus aliados le han proporcionado ", 0.8));
        cartas.add(new CartaIrCasilla("El senado le ha solicitado que se presenta en ", 5));
        cartas.add(new CartaIrCasilla("El senado le ha solicitado que se presenta en ", 35));
        cartas.add(new CartaIrCasilla("El senado le ha solicitado que se presenta en ", 26));
        cartas.add(new CartaIrCasilla("El senado le ha solicitado que se presenta en ", 14));
        cartas.add(new CartaIrCasilla("El senado le ha solicitado que se presenta en ", 36));
        cartas.add(new CartaIrCasilla("El senado le ha solicitado que se presenta en ", 2));
        cartas.add(new CartaIrCasilla("El senado le ha solicitado que se presenta en ", 22));
        cartas.add(new CartaIrCasilla("El senado le ha solicitado que se presenta en ", 17));
        
        Mesa.salvarCartas(cartas);
    }
    
}
