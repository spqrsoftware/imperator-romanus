/*
 * main.java
 *
 * Created on 10 de marzo de 2006, 10:57 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package sistema;

import codigo.*;
import codigo.cartas.*;
import codigo.casillas.*;

public class main {
    
    public main() {
    }
    
    public static void main(String[] args) {
        Casilla casillas [] = new Casilla [37];
        
        casillas [0] = new Senado();
        casillas [1] = new Region("Brutium", "Crot�n",  5);
        casillas [2] = new Region("Apulia", "Tarentum", 3);
        casillas [3] = new Region("Aetolia", "Thermon", 4);
        casillas [4] = new CasillaCarta();
        casillas [5] = new Region("Umbria", "Ariminun", 8);
        casillas [6] = new Region("Etruria", "Arretium", 8);
        casillas [7] = new Region("Venetia", "Patavium", 6);
        casillas [8] = new Region("Liguria", "Segesta",4 );
        casillas [9] = new Region("Epirus", "Apolonia", 6);
        casillas [10] = new Region("Aquitania", "Lamonum", 5);
        casillas [11] = new Gladiator();
        casillas [12] = new Region("Lugdinensis", "Lugdumun", 9);
        casillas [13] = new Region("Narbonensis", "Narbo", 7);
        casillas [14] = new Region("Taraconesis", "Osca", 5);
        casillas [15] = new Region("Celtiberia", "Numantia", 1);
        casillas [16] = new Region("Peloponesus", "Corinto", 3);
        casillas [17] = new Region("Aquitania", "Lemonum", 5);
        casillas [18] = new Region("Armorica", "Redonum", 5);
        casillas [19] = new BanosPublicos();
        casillas [20] = new Region("B�lgica", "Samarobriva", 5);
        casillas [21] = new Region("Cicilia", "Tarsus", 2);
        casillas [22] = new Region("Assyria", "Hatra", 6);
        casillas [23] = new Region("Africa", "Cartago", 9);
        casillas [24] = new Coliseum();
        casillas [25] = new Region("Judaea", "Jerusalem", 1);
        casillas [26] = new Region("Boihaemun", "Lovosice", 8);
        casillas [27] = new Region("Germania Supra", "Trier", 3);
        casillas [28] = new CasillaCarta();
        casillas [29] = new Region("Saxones", "Bordesholm", 5);
        casillas [30] = new Region("Locus Gothi", "Vicus Gothi", 7);
        casillas [31] = new Region("Pripet", "Venedae", 10);
        casillas [32] = new Region("Dacia", "Porrolissum", 12);
        casillas [33] = new Region("Thrace", "Tylis", 10);
        casillas [34] = new Region("Propontis", "Byzantium", 11);
        casillas [35] = new Region("�taca", "Atenas", 14);
        casillas [36] = new Region("Ba�tica", "C�rdoba", 15);
        
        Mesa.instanciar(0);
        
        Mesa.agregarCasillas(casillas);
        Mesa.salvarRegiones();
    }
    
}
