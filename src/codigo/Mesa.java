package codigo;

import codigo.cartas.*;
import estructuras.*;
import java.awt.Color;
import java.io.*;
import java.util.ArrayList;
import javax.swing.*;

public class Mesa{
    
    private static Casilla [] casillas;
    private static Pila cartas;
    private static Cola jugadores;
    private static ListaOrdenada vitacora1;
    private static ListaOrdenada vitacora2;
    private static ListaOrdenada vitacora3;
    private static int pos_brutii;
    private static int pos_scipii;
    private static int pos_julii;
    private static MejoresJugadores mj;
    private static final String ARCHIVO_CARTAS = "cartas.ir1";
    private static final String ARCHIVO_CASILLAS = "casillas.ir1";
    private static final String MOVIMIENTO_JUGADOR1 = "movjug1.ir1";
    private static final String MOVIMIENTO_JUGADOR2 = "movjug2.ir1";
    private static final String MOVIMIENTO_JUGADOR3 = "movjug3.ir1";
    private static final Color VERDE = new Color(50, 100, 50 );
    private static final Color ROJO = new Color( 150, 0, 0 );
    private static final Color AZUL = new Color( 50, 50, 100 );
    private static Jugada jug;
    
    
//********************  Constructor privado de Mesa  *************************//
    
    private Mesa() {
    }   //fin del constructor default
    
//************************* M�todos para agregar  ****************************//
    
    public static void agregarJugada(){
        if( jugadorActual().getEmperador().getFamilia().equalsIgnoreCase( "Brutii")){
            vitacora1.agregar(getJug());
        }   //final del if
        if( jugadorActual().getEmperador().getFamilia().equalsIgnoreCase( "Julii")){
            vitacora2.agregar(getJug());
        }   //final del if
        if( jugadorActual().getEmperador().getFamilia().equalsIgnoreCase( "Scipii")){
            vitacora3.agregar(getJug());
        }   //final del if
    }
    
    public static void nuevaJugada(){
        jug = new Jugada();
    }
    
    public static void agregarJugador( Jugador j ){
        jugadores.agregar( j );
    }   //fin del m�todo agregarJugador
    
    public static void agregarCasillas( Casilla [] ca ){
        for (int i = 0; (i < ca.length) || (i < casillas.length); i++){
            casillas[i] = (ca[i]);
        }
    }   //fin del m�todo agregarJugador
    
    public static void barajar( ArrayList ca ){
        while (ca.size() != 0){
            int num = (int) (Math.random() * ca.size());
            Carta temp = (Carta) ca.get(num);
            ca.remove(num);
            cartas.push(temp);
        }
    }   //fin del m�todo agregarJugador
    
//*****************************  M�todo Instanciar  **************************//
    
    /**M�todo para instanciar los atributos de la clase Mesa*/
    public static void instanciar(){
        casillas = new Casilla [37];
        setCartas(new Pila());
        jugadores = new Cola();
        mj = new MejoresJugadores();
        vitacora1 = new ListaOrdenada();
        vitacora2 = new ListaOrdenada();
        vitacora3 = new ListaOrdenada();
        cargar();
    }   //fin del m�todo instanciar
    
    /**M�todo para instanciar los atributos de la clase Mesa*/
    public static void instanciar(int a){
        casillas = new Casilla [37];
        setCartas(new Pila());
        jugadores = new Cola();
    }   //fin del m�todo instanciar
    
//********************  M�todos para controlar los turnos  *******************//
    
    public static Jugador jugadorActual(){
        return jugadores.jugadorActual();
    }   //fin del m�todo jugadorActual
    
    public static Jugador jugadorSiguiente(){
        return jugadores.siguienteJugador();
    }   //fin del m�todo jugadorSiguiente
    
//***********************  M�todo para tirar los dados  **********************//
    
    public static int tirarDados(){
        return Dado.tirar();
    }   //fin del m�todo tirarDados
    
//****************************** Sets y gets  ********************************//
    
    // <editor-fold defaultstate="collapsed" desc=" Sets y Gets ">
    public static int getPos_brutii() {
        return pos_brutii;
    }   //fin de m�todo getPos_brutii
    
    public static void setPos_brutii(int aPos_brutii) {
        if( aPos_brutii > 36 ){
            aPos_brutii -= 36;
            pos_brutii = aPos_brutii;
        }else{
            pos_brutii = aPos_brutii;
        }   //fin del if...else
    }   //fin del m�todo setPos_brutii
    
    public static int getPos_scipii() {
        return pos_scipii;
    }   //fin del m�todo getPos_scipii
    
    public static void setPos_scipii(int aPos_scipii) {
        if( aPos_scipii > 36 ){
            aPos_scipii -= 36;
            pos_scipii = aPos_scipii;
        }else{
            pos_scipii = aPos_scipii;
        }   //fin del if...else
    }   //fin del m�todo setPos_scipii
    
    public static int getPos_julii() {
        return pos_julii;
    }   //fin del m�todo getPos_julii
    
    public static void setPos_julii(int aPos_julii) {
        if( aPos_julii > 36 ){
            aPos_julii -= 36;
            pos_julii = aPos_julii;
        }else{
            pos_julii = aPos_julii;
        }   //fin del if...else
    }   //fin del m�todo setPos-julii
    
    public static Cola getJugadores(){
        return jugadores;
    }   //fin del m�todo getJugadores
    
    public static Casilla [] getCasillas(){
        return casillas;
    }   //fin del m�todo getCasillas
    
    public static Pila getCartas() {return cartas;}
    public static void setCartas(Pila aCartas) {cartas = aCartas;}
    // </editor-fold>
    
    public static ListaOrdenada getVitacora(String familia){
        if( familia.equalsIgnoreCase( "Brutii")){
            return vitacora1;
        }   //final del if
        if( familia.equalsIgnoreCase( "Julii")){
            return vitacora2;
        }   //final del if
        if( familia.equalsIgnoreCase( "Scipii")){
            return vitacora3;
        }   //final del if
        return null;
    }
    
//**************** M�todos relacionados a las casillas  **********************//
    
    private static void pelear( Imperator imp, Region r ){
        int temp_imp = imp.getEjercito().size();
        int temp_r = r.getEjercito().size();
        if( r.getEmperador().equals( imp ) ){
        }else{
            if( imp.getEjercito().size() < r.getEjercito().size() ){
                for( int i = 0; i < temp_imp; i++ ){
                    imp.getEjercito().remove(0);
                    r.getEjercito().remove(0);
                }   //fin del for
            }else{
                for( int i = 0; i < temp_r; i++ ){
                    imp.getEjercito().remove( 0 );
                    r.getEjercito().remove( 0 );
                }   //fin del for
                r.getEmperador().quitarRegionDeDominio(r);
                r.setEmperador( imp );
                imp.agregarRegionADominio(r);
            }   //fin del if...else anidado
        }   //fin del if...else
    }   //fin del m�todo pelear
    
    public static void conquistarRegion( Imperator imp, Region r ){
        if( r.getEmperador()== null ){
            r.setEmperador( imp );
            imp.agregarRegionADominio(r);
        }else{
            pelear( imp,  r );
        }   //fin del else
    }   //fin del m�todo conquistarRegion
    
//********************  M�todo para calcular la regi�n  **********************//
    
    public static Region calcularRegion( int pos ){
        Casilla temp = casillas[ pos ];
        if( temp instanceof Region ){
            Region r = ( Region ) temp;
            return r;
        }else{
            calcularCasilla( pos );
        }   //fin del if...else
        return null;
    }   //fin del m�todo CalcularRegi�n
    
    private static Casilla calcularCasilla( int pos ){
        return casillas[ pos ];
    }   //fin del m�todo calcularCasilla
    
//***************  M�todo para manejar a los mejores jugadores  **************//
    
    public static void agregarMejorJugador( Jugador j ){
        mj.agregar(j);
    }   //fin del m�todo agregarMejorJugador
    
//************************ M�todos para manejo de flujos **********************//
    
    private static void cargar(){
        cargarRegiones();
        cargarCartas();
    }
    
    private static void cargarRegiones(){
        ObjectInputStream entrada = null;
        try{
            entrada = new ObjectInputStream(new FileInputStream(ARCHIVO_CASILLAS));
        }catch (IOException e){
            JOptionPane.showMessageDialog(null, "Error de apertura", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        
        boolean seguir = true;
        
        try{
            for (int i=0; seguir; i++){
                Casilla temp = (Casilla) entrada.readObject();
                casillas[i] = (temp);
            }
        }catch (EOFException e){
            seguir = false;
        }catch(ClassNotFoundException ex){
            JOptionPane.showMessageDialog(null, "Error de Casting", "ERROR", JOptionPane.ERROR_MESSAGE);
        }catch (IOException e){
            JOptionPane.showMessageDialog(null, "Error de lectura", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        
        try{
            entrada.close();
        }catch( IOException e ){
            JOptionPane.showMessageDialog( null, "Error de cierre", "Error", JOptionPane.WARNING_MESSAGE );
        }
    }   //fin del m�todo cargar
    
    private static void cargarCartas(){
        ArrayList t = new ArrayList();
        ObjectInputStream entrada = null;
        try{
            entrada = new ObjectInputStream(new FileInputStream(ARCHIVO_CARTAS));
        }catch (IOException e){
            JOptionPane.showMessageDialog(null, "Error de apertura", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        
        boolean seguir = true;
        
        try{
            for (int i=0; seguir; i++){
                Carta temp = (Carta) entrada.readObject();
                t.add(temp);
            }
        }catch (EOFException e){
            seguir = false;
        }catch(ClassNotFoundException ex){
            JOptionPane.showMessageDialog(null, "Error de Casting", "ERROR", JOptionPane.ERROR_MESSAGE);
        }catch (IOException e){
            JOptionPane.showMessageDialog(null, "Error de lectura", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        
        try{
            entrada.close();
        }catch( IOException e ){
            JOptionPane.showMessageDialog( null, "Error de cierre", "Error", JOptionPane.WARNING_MESSAGE );
        }
        barajar(t);
    }   //fin del m�todo cargar
    
    public static void salvarRegiones(){
        try{
            ObjectOutputStream salida = new ObjectOutputStream(new FileOutputStream(ARCHIVO_CASILLAS));
            
            for (int i = 0; i<37; i++){
                salida.writeObject(casillas[i]);
            }
            salida.close();
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }   //fin del m�todo salvarRegiones
    
    public static void salvarCartas(ArrayList arreglo){
        try{
            ObjectOutputStream salida = new ObjectOutputStream(new FileOutputStream(ARCHIVO_CARTAS));
            
            for (int i = 0; i < arreglo.size(); i++){
                CasillaCarta ca = (CasillaCarta) arreglo.get(i);
                salida.writeObject(ca);
            }
            salida.close();
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }   //fin del m�todo salvarRegiones
    
    public static Jugada getJug() {
        return jug;
    }
    
}   //fin de la claseMesa