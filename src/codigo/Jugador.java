package codigo;

import excepciones.CampoVacioException;
import java.io.*;

/**Clase que guarda los datos del jugador.*/
public class Jugador implements Serializable{
    
    private String nombre;
    private Imperator emperador;
    private int numero_vueltas;
    
//****************************  Constructores  *******************************//
    public Jugador() throws CampoVacioException{
        setNombre(null);
        setEmperador(null);
        setNumero_vueltas( -1 );
    }   //fin del constructor
    
    public Jugador( String nombre, Imperator imp ) throws CampoVacioException{
        setNombre( nombre );
        setEmperador( imp );
        setNumero_vueltas( 0 );
    }   //fin de constructor
    
    public void agregarVuelta(){
        setNumero_vueltas( getNumero_vueltas() + 1 );
    }   //fin del m�todo agregarVueltas
    
//*********************************  Sets  ***********************************//
    private void setNumero_vueltas(int numero_regiones){this.numero_vueltas = numero_regiones;}
    public void setNombre(String nombre) throws CampoVacioException{
        if( nombre.trim().length() != 0 ){
            this.nombre = nombre;
        }else{
            throw new CampoVacioException();
        }   //fin del if...else
    }   //fin del m�todo setNombre
    public void setEmperador(Imperator emperador) {this.emperador = emperador;}
    
//*********************************  Gets  ***********************************//
    public String getNombre(){return nombre;}
    public int getNumero_vueltas() {return numero_vueltas;}
    public Imperator getEmperador(){return emperador;}
    
    public String toString(){
        return ("Nombre: " + getNombre() + "\nEmperador: " + getEmperador().getNombre()
        + " " + getEmperador().getFamilia() );
    }
    
}   //fin de la clase Jugador