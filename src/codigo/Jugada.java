package codigo;

public class Jugada {
    
    private int turno;
    private boolean peleo;
    private int dado1;
    private int dado2;
    private String regionActual;
    
    public Jugada() {
        setTurno(Mesa.jugadorActual().getEmperador().getTurno());
    }
    
    public Jugada(boolean peleo, int dado1, int dado2, String regionActual) {
        this();
        setPeleo(peleo);
        setDado1(dado1);
        setDado2(dado2);
        setRegionActual(regionActual);
    }
    
    public Jugada(boolean peleo, int dado1, int dado2, Casilla regionActual) {
        this();
        setPeleo(peleo);
        setDado1(dado1);
        setDado2(dado2);
        setRegionActual(regionActual);
    }
    
    // <editor-fold defaultstate="collapsed" desc=" set's ">
    public void setTurno(int turno) {
        this.turno = turno;
    }
    
    public void setPeleo(boolean peleo) {
        this.peleo = peleo;
    }
    
    public void setDado1(int dado1) {
        this.dado1 = dado1;
    }
    
    public void setDado2(int dado2) {
        this.dado2 = dado2;
    }
    
    public void setRegionActual(String regionActual) {
        this.regionActual = regionActual;
    }
    
    public void setRegionActual(Casilla regionActual) {
        try{
            this.regionActual = regionActual.getNombre();
        }catch (NullPointerException e){
            this.regionActual = null;
        }
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" get's ">
    public int getTurno() {
        return turno;
    }
    
    public boolean isPeleo() {
        return peleo;
    }
    
    public int getDado1() {
        return dado1;
    }
    
    public int getDado2() {
        return dado2;
    }
    
    public String getRegionActual() {
        return regionActual;
    }
    //</editor-fold>
    
    public String toString(){
        String msn = "";
        msn += "Turno: " + getTurno();
        msn += "\n" + (isPeleo()?"Si": "No") + " peleo";
        msn += "\nDado1: " + getDado1() + ", Dado 2: " + getDado2();
        msn += "\nRegi�n actual: " + getRegionActual() + "\n";
        return msn;
    }
    
}
