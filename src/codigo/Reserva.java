package codigo;

import java.util.*;
import java.io.*;

public class Reserva implements Serializable{
    
    private String comandante;
    private ArrayList lista_soldados;
    
//*****************************  Constructores  ******************************//
    
    public Reserva() {
        lista_soldados = null;
        setComandante( null );
    }   //fin del constructor
    
    public Reserva( String comandante ){
        setComandante( comandante );
        lista_soldados = new ArrayList();
    }   //fin de constructor
    
//*****************************  Otros M�todos  ******************************//
    
    public void agregar( Soldado s ){
        lista_soldados.add( s );
    }   //fin del m�todo agregar
    
    /**A�ade la cantidad de soldados solicitados a un ArrayList y luego los devuelve*/
    public ArrayList sacar( int num ){
        try{
            ArrayList temp = new ArrayList();
            for( int i = 0; i < num; i++ ){
                temp.add( ((Soldado)lista_soldados.get( i )) );
                lista_soldados.remove( i );
            }   //fin del for
            return temp;
        }catch( Exception e ){
            return null;
        }   //fin del try & catch
    }   //fin del m�todo sacar
    
//*****************************  Sets y Gets  ********************************//
    
    public String getComandante(){return comandante;}
    public void setComandante(String comandante){this.comandante = comandante;}
    
    public ArrayList getLista_soldados(){return lista_soldados;}
    public void setLista_soldados(ArrayList lista_soldados){this.lista_soldados = lista_soldados;}
    
}   //fin de la clase Reserva