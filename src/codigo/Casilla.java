package codigo;

import java.io.*;

public class Casilla implements Serializable{
    
    private String nombre;
    
    public Casilla() {
        setNombre("Sin asignar");
    }
    
    public Casilla(String nombre) {
        setNombre(nombre);
    }
    
    public String getNombre(){return nombre;}
    public void setNombre(String nombre){this.nombre = nombre;}
    
}