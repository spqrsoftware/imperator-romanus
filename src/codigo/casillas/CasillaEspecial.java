package codigo.casillas;

import codigo.*;
import java.io.*;

public abstract class CasillaEspecial extends Casilla implements Serializable{
    
    public CasillaEspecial(){
        super();
    }
    
    public CasillaEspecial(String texto){
        super(texto);
    }
    
    public abstract void efecto();
    
}   //fin dela interface CasillaEspecial