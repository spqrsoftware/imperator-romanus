package codigo.casillas;

import codigo.*;

public class Coliseum extends CasillaEspecial{
    
    public Coliseum() {
        super("Coliseum");
    }
    
    public void efecto(){
        Mesa.jugadorActual().getEmperador().setEncarcelado(true);
    }
    
}