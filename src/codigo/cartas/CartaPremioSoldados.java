package codigo.cartas;

import codigo.*;
import java.util.ArrayList;

public class CartaPremioSoldados extends CasillaCarta{
    
    private double factor;
    
    public CartaPremioSoldados() {
        super("Sin texto");
        factor = 1;
    }
    
    public CartaPremioSoldados(String texto, double numero) {
        super(texto);
        this.factor = numero;
    }
    
    public void efecto(){
    }
    
    private int calcular(){
        ArrayList ejercito = Mesa.jugadorActual().getEmperador().getReserva().getLista_soldados();
        return (int) (ejercito.size() * factor);
    }
    
    public int efecto(int pos){
        Imperator imp = Mesa.jugadorActual().getEmperador();
        ArrayList reserva = imp.getReserva().getLista_soldados();
        int num = calcular();
        for (int i = 0; i < num; i++){
            reserva.add(new Soldado(imp));
        }
        imp.getReserva().setLista_soldados(reserva);
        return pos;
    }
    
    public String toString(){
        return getTexto() + calcular() + " nuevos soldados.";
    }
    
}