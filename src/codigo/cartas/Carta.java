package codigo.cartas;

import codigo.casillas.*;

public abstract class Carta extends CasillaEspecial{
    
    private String texto;
    
    public Carta(){
        setTexto("Sin texto");
    }
    
    public Carta(String texto){
        setTexto(texto);
    }
    
    public void efecto(){}
    
    public abstract int efecto(int posicion);
    
    public String getTexto() {return texto;}
    public void setTexto(String texto) {this.texto = texto;}
    
}   //fin de lainterface Carta