package codigo.cartas;

import codigo.Mesa;

public class CartaAnularMov extends CasillaCarta{
    
    public CartaAnularMov() {
        super("Sin texto");
    }
    
    public CartaAnularMov(String texto) {
        super(texto);
    }
    
    public void efecto(){
    }
    
    public int efecto(int pos){
        String familia = Mesa.jugadorActual().getEmperador().getFamilia();
        if (familia.equalsIgnoreCase("brutii")){
            return Mesa.getPos_brutii();
        }else if(familia.equalsIgnoreCase("julii")){
            return Mesa.getPos_julii();
        }else if(familia.equalsIgnoreCase("scipii")){
            return Mesa.getPos_scipii();
        }
        return pos;
    }
    
    public String toString(){
        return getTexto();
    }
    
}