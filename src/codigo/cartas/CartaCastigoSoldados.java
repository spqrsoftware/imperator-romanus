package codigo.cartas;

import codigo.*;
import java.util.ArrayList;

public class CartaCastigoSoldados extends CasillaCarta{
    
    private boolean delEjercito;
    private int divisor;
    
    public CartaCastigoSoldados() {
        super("Sin texto");
        delEjercito = false;
        divisor = 0;
    }
    
    public CartaCastigoSoldados(String texto, boolean bandera, int numero) {
        super(texto);
        delEjercito = bandera;
        this.divisor = numero;
    }
    
    public void efecto(){
    }
    
    private int calcularMitad(){
        ArrayList ejercito = Mesa.jugadorActual().getEmperador().getEjercito();
        return (int) (ejercito.size() / divisor);
    }
    
    public int efecto(int pos){
        Imperator imp = Mesa.jugadorActual().getEmperador();
        if (delEjercito){
            ArrayList ejercito = imp.getEjercito();
            for (int i = 0; i < calcularMitad(); i++){
                ejercito.remove(0);
            }
            imp.setEjercito(ejercito);
        }else{
            ArrayList reserva = imp.getReserva().getLista_soldados();
            for (int i = 0; i < calcularMitad(); i++){
                reserva.remove(0);
            }
            imp.getReserva().setLista_soldados(reserva);
        }
        return pos;
    }
    
    public String toString(){
        return getTexto() + calcularMitad() + " soldados.";
    }
    
}
