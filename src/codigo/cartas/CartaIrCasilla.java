package codigo.cartas;

import codigo.*;

public class CartaIrCasilla extends CasillaCarta{
    
    private int posicion;
    
    public CartaIrCasilla() {
        super("Sin texto");
        posicion = 0;
    }
    
    public CartaIrCasilla(String texto, int posicion) {
        super(texto);
        this.posicion = posicion;
    }
    
    public void efecto(){
    }
    
    public int efecto(int pos){
        return posicion;
    }
    
    private String obtenerNombre(){
        Casilla [] ca = Mesa.getCasillas();
        Casilla temp = ca [posicion];
        if (!temp.getNombre().equalsIgnoreCase("Sin asignar")){
            return temp.getNombre();
        }
        return "";
    }
    
    public String toString(){
        return getTexto() + obtenerNombre();
    }
    
}