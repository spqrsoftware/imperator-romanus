package codigo.cartas;

public class CartaCarcel extends CasillaCarta{
    
    public CartaCarcel() {
        super("Sin texto");
    }
    
    public CartaCarcel(String texto) {
        super(texto);
    }
    
    public void efecto(){
    }
    
    public int efecto(int pos){
        return 24;
    }
    
    public String toString(){
        return getTexto();
    }
    
}