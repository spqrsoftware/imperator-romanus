package codigo.cartas;

public class CasillaCarta extends Carta{
    
    public CasillaCarta() {
        super("Sin texto");
    }
    
    public CasillaCarta(String texto) {
        super(texto);
    }
    
    public void efecto(){
    }
    
    public int efecto(int pos){
        return 0;
    }
    
}