package codigo;

import java.util.ArrayList;
import java.io.*;

public class Region extends Casilla implements Serializable{
    
    private String ciudad;
    private Imperator emperador;
    private ArrayList ejercito;
    private int soldadosGenerados;
    
//******************************  Constructores  *****************************//
    
    public Region() {
        super( "Sin asignar" );
        setCiudad( "Sin asginar" );
        setEmperador( null );
        setEjercito( null );
        setSoldadosGenerados(0);
    }   //fin del constructor
    
    public Region( String nombre, String ciudad, int numeroSoldados ){
        super( nombre );
        setCiudad( ciudad );
        setEmperador( null );
        setEjercito( new ArrayList() );
        setSoldadosGenerados(numeroSoldados);
    }   //fin de constructor
    
//****************************  Otros m�todos  *******************************//
    
    public void agregarSoldado( Soldado s ){
        getEjercito().add( s );
    }   //fin del m�todo agregarSoldado
    
    public ArrayList devolverSoldado( int cantidad ) throws Exception{
        if( cantidad <= getEjercito().size() ){
            ArrayList temp = new ArrayList();
            for( int i = 0; i < cantidad; i++ ){
                temp.add( ((Soldado) getEjercito().get( i )) );
                getEjercito().remove( i );
            }   //fin del for
            return temp;
        }else{
            throw new Exception( "No tiene " + cantidad + " soldados en esta regi�n" );
        }   //fin del if...else
    }   //fin del m�todo devolverSoldado
    
    public void producirSoldados(){
        if( getEmperador() != null ){
            if( !getEmperador().isEncarcelado() ){
                for( int i = 0; i <= soldadosGenerados; i++ ){
                    getEmperador().agregarSoldadoReserva( new Soldado( getEmperador() ) );
                }//fin del for
            }   //fin del if
        }   //fin del if
    }   //fin del m�todo producir soldados
    
//*****************************  Sets y gets  ********************************//
    
    public String getCiudad(){return ciudad;}
    public void setCiudad(String ciudad){this.ciudad = ciudad;}
    
    public Imperator getEmperador(){return emperador;}
    public void setEmperador(Imperator emperador){this.emperador = emperador;}
    
    public ArrayList getEjercito(){return ejercito;}
    public void setEjercito(ArrayList ejercito){this.ejercito = ejercito;}
    
    public int getSoldadosGenerados() {return soldadosGenerados;}
    public void setSoldadosGenerados(int soldadosGenerados) {this.soldadosGenerados = soldadosGenerados;}
    
}   //fin de la clase Region