package codigo;

import java.util.ArrayList;
import java.io.*;

public class Imperator implements Serializable{
    
    private String familia;
    private String nombre;
    private boolean encarcelado;
    private ArrayList ejercito;
    private ArrayList dominio;
    private Reserva reserva;
    private int turnosCarcel;
    private int turno;
    
//***************************  Constructores  ********************************//
    
    public Imperator() {
        setFamilia( "Maximus" );
        setNombre( "Pious" );
        setEncarcelado( false );
        setEjercito( null );
        setDominio( null );
        setReserva( null );
    }   //fin del constructor
    
    public Imperator( String nombre, String familia ){
        setNombre( nombre );
        setFamilia( familia );
        setEncarcelado( false );
        setEjercito( new ArrayList() );
        setDominio( new ArrayList() );
        setReserva( new Reserva( nombre ) );
    }   //fin de constructor
    
//**************************** M�todos de reserva  ***************************//
    /**M�todo que quita soldados de la lista de ejercito y los mueve a la reserva*/
    public void reservarSoldados( int cantidad ) throws Exception{
        if( cantidad <= getEjercito().size() ){
            for( int i = 0; i <= cantidad; i++ ){
                agregarSoldadoReserva( ((Soldado) getEjercito().get( 0 )) );
                getEjercito().remove( 0 );
            }   //fin del for
            
        }else{
            throw new Exception( "No tiene suficientes soldados en el ejercito para reservar" );
        }   //fin del if...else
    }   //fin del m�todo reservarSoldados
    
    /**M�todo que quita soldados de la reserva y los mueve a la lista de ejercito*/
    public void pedirSoldadosAReserva( int cantidad ) throws Exception{
        if( cantidad <= getReserva().getLista_soldados().size() ){
            for( int i = 0; i <= cantidad; i++ ){
                agregarSoldadoEjercito( ((Soldado) getReserva().getLista_soldados().get( 0 )) );
                getReserva().getLista_soldados().remove( 0 );
            }   //fin del for
            
        }else{
            throw new Exception( "No tiene suficientes soldados en la reserva para llamar" );
        }   //fin del if...else
    }   //fin del m�todo pedirSoldadosAReserva
    
//***************  M�todos para dejar soldados en una regi�n  ****************//
    /**M�todo que deja un soldado, tomado de la lista de ej�rcito, en una regi�n*/
    public void dejarSoldadoEjercito( Region r, int cantidad ) throws Exception{
        if( r.getEmperador().getFamilia().equals( getFamilia() )){
            if( cantidad <= getEjercito().size() ){
                for( int i = 0; i <= cantidad; i ++ ){
                    r.agregarSoldado( ((Soldado)getEjercito().get(0)) );
                    getEjercito().remove( 0 );
                }   //fin del for
                
            }else{
                throw new Exception( "No tiene suficientes soldados en el " +
                        "ejercito para dejar en la regi�n" );
            }   //fin del if anidado
        }   //fin del if
    }   //fin del m�todo dejarSoldadoEjercito
    
    /**M�todo que deja un soldado, tomado de la reserva, en una regi�n*/
    public void dejarSoldadoReserva( Region r, int cantidad ) throws Exception{
        if( r.getEmperador().getNombre().equals( getNombre() )){
            if( cantidad <= getReserva().getLista_soldados().size() ){
                for( int i = 0; i <= cantidad; i ++ ){
                    r.agregarSoldado( ((Soldado)getReserva().getLista_soldados().get(0)) );
                    getReserva().getLista_soldados().remove( 0 );
                }   //fin del for
                
            }else{
                throw new Exception( "No tiene suficientes soldados en la " +
                        "reserva para dejar en la regi�n" );
            }   //fin del if anidado
        }   //fin del if
    }   //fin del m�todo dejarSoldadoReserva
    
//***************  M�todos para retirar soldados de una regi�n  **************//
    
    /**M�todo que retira a un soldado de una regi�n y lo mueve al ej�rcito*/
    public void retirarSoldadoAEjercito( Region r, int cantidad ) throws Exception{
        if( r.getEmperador().getNombre().equals( getNombre() ) ){
            if( cantidad <= r.getEjercito().size() ){
                for( int i = 0; i <= cantidad; i++ ){
                    getEjercito().add( ((Soldado) r.getEjercito().get(0)) );
                    r.getEjercito().remove(0);
                }   //fin del for
                
            }else{
                throw new Exception( "No tiene suficientes soldados en esta regi�n" );
            }   //fin del if...else anidado
        }   //fin del if
    }   //fin del m�todo retirarSoldadoAEjercito
    
    /**M�todo que retira a un soldado de una regi�n y lo mueve a la reserva*/
    public void retirarSoldadoAReserva( Region r, int cantidad ) throws Exception{
        if( r.getEmperador().getNombre().equals( getNombre() ) ){
            if( cantidad <= r.getEjercito().size() ){
                for( int i = 0; i <= cantidad; i++ ){
                    getReserva().agregar(((Soldado) r.getEjercito().get(0)) );
                    r.getEjercito().remove(0);
                }   //fin del for
                
            }else{
                throw new Exception( "No tiene suficientes soldados en esta regi�n" );
            }   //fin del if...else anidado
        }   //fin del if
    }   //fin del m�todo retirarSoldadoAReserva
    
//****************************  M�todos agregar  *****************************//
    
    public void agregarSoldadoEjercito( Soldado s ){
        ejercito.add( s );
    }   //fin del m�todo agregarSoldadoEjercito
    
    public void agregarSoldadoReserva( Soldado s ){
        reserva.agregar( s );
    }   //fin del m�todo agregarSoldadoReserva
    
//***********************  M�todos sobre el dominio  *************************//
    /**Agrega una regi�n al dominio del emperador*/
    public void agregarRegionADominio( Region r ){
        dominio.add(r);
    }   //fin del m�todo agregarRegionADominio
    
    public void quitarRegionDeDominio( Region r ){
        dominio.remove(r);
    }   //fin del m�todo quitarRegionDominio
    
    public void validaEncarcelado(){
        if (getTurno() >= turnosCarcel){
            setEncarcelado(false);
        }
    }
    
    public void aumentarTurno(){
        setTurno(getTurno() + 1);
    }
    
//******************************  Sets y gets  *******************************//
    
    public String getFamilia(){return familia;}
    public void setFamilia(String familia){this.familia = familia;}
    
    public String getNombre(){return nombre;}
    public void setNombre(String nombre){this.nombre = nombre;}
    
    public boolean isEncarcelado(){return encarcelado;}
    public void setEncarcelado(boolean encarcelado){
        this.encarcelado = encarcelado;
        setTurnosCarcel(3);
    }
    
    public ArrayList getEjercito(){return ejercito;}
    public void setEjercito(ArrayList ejercito){this.ejercito = ejercito;}
    
    public ArrayList getDominio(){return dominio;}
    public void setDominio(ArrayList dominio){this.dominio = dominio;}
    
    public Reserva getReserva(){return reserva;}
    public void setReserva(Reserva reserva){this.reserva = reserva;}
    
    public int getTurnosCarcel() {return turnosCarcel;}
    public void setTurnosCarcel(int numero) {
        this.turnosCarcel = getTurno() + numero;
    }
    
    public int getTurno() {
        return turno;
    }
    
    private void setTurno(int turno) {
        this.turno = turno;
    }
    
}   //fin de la clase Imperator