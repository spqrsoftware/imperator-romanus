package codigo;

import java.io.*;

public class Soldado implements Serializable{
    
    private Imperator comandante;
    private boolean reservado;
    
    /**
     * Crea una nueva instancia Soldado
     */
    public Soldado() {
        setComandante( null );
        setReservado( false );
    }   //fin del constructor
    
    public Soldado( Imperator imp ){
        setComandante( imp );
        setReservado( true );
    }   //fin de constructor
    
//***************************  Sets y Gets  **********************************//
    
    public Imperator getComandante() {return comandante;}
    public void setComandante(Imperator comandante){this.comandante = comandante;}
    
    public boolean isReservado(){return reservado;}
    public void setReservado(boolean reservado){this.reservado = reservado;}
    
}   //fin de laclase Soldado