package estructuras;

import excepciones.*;
import java.io.*;

public class Pila {
    
    private Nodo cabeza;
    
//***************************  Constructores  ********************************//
    
    public Pila() {
    }   //fin del constrctor default
    
//**************************** Otros m�todos  ********************************//
    public void push( Object obj ){
        Nodo temp = new Nodo();
        temp.setInfo( obj );
        if( cabeza == null ){
            cabeza = temp;
        }else{
            temp.setSig( cabeza );
            cabeza = temp;
        }   //fin del if...else
    }   //fin del m�todo push
    
    public Object pop() throws ListaVaciaException{
        Nodo temp;
        if( cabeza != null ){
            temp = cabeza;
            cabeza = cabeza.getSig();
            Object objeto = temp.getInfo();
            temp.setInfo( null );
            temp.setSig( null );
            temp = null;
            return objeto;
        }else{
            throw new ListaVaciaException("No se ha podido borrar");
        }   //fin del if...else
    }   //fin del m�todo pop
    
//**************************** Sets y gets  **********************************//
    public Nodo getCabeza(){return cabeza;}
    public void setCabeza(Nodo cabeza){this.cabeza = cabeza;}
    
    public void guardar(String nombreArchivo) throws FlujoException, ListaVaciaException{
        ObjectOutputStream salida=null;
        try{
            salida = new ObjectOutputStream(new FileOutputStream(nombreArchivo));
        }catch(IOException e){
            throw new FlujoException("ERROR DE APERTURA");
        }
        Nodo iterador = cabeza;
        
        while(iterador !=null){
            // Tratar de guardar el elemento en el archivo abierto
            try{
                salida.writeObject(iterador.getInfo());
                salida.flush();
            }catch(IOException e){
                throw new FlujoException("ERROR AL GUARDAR");
            }
            // hacer iterador igual al siguiente
            this.pop();
            iterador = getCabeza();
        }
        try{
            salida.close();
        }catch(IOException e){
            throw new FlujoException("ERROR AL CERRAR ARCHIVO");
        }
    }
    
    public void cargar(String nombreArchivo) throws FlujoException{
        ObjectInputStream entrada=null;
        try{
            entrada = new ObjectInputStream(new FileInputStream(new File(nombreArchivo)));
        }catch(IOException e){
            throw new FlujoException("ERROR DE APERTURA");
        }
        Nodo iterador = cabeza;
        Object dato = null;
        do{
            // Tratar de cargar el elemento desde el archivo abierto
            try{
                dato = entrada.readObject();
                this.push(dato);// Insertar al final
            }catch(EOFException e){
                dato = null;
            }catch(IOException e){
                throw new FlujoException("ERROR AL CARGAR");
            }catch (ClassNotFoundException e){
                throw new FlujoException("No se encontr� la clase");
            }
            // hacer iterador igual al siguiente
        } while(dato!=null);
    }
    
}   //fin de la clase Pila