package estructuras;

import codigo.*;

public class ListaOrdenada {
    
    private Nodo cabeza;
    
    public ListaOrdenada() {
    }
    
    /***************************************** Metodos *****************************************/
    
    public Nodo eliminar(){
        Nodo temp = cabeza;
        cabeza = cabeza.getSig();
        temp.setSig(null);
        return temp;
    }
    
    public void insertarInicio(Object objeto){
        Nodo temp = new Nodo();
        temp.setInfo(objeto);
        temp.setSig(cabeza);
        cabeza = temp;
    }
    
    public void agregar(boolean peleo, int dado1, int dado2, String regionActual){
        Jugada jug = new Jugada(peleo, dado1, dado2, regionActual);
        agregar(jug);
    }
    
    public void agregar(boolean peleo, int dado1, int dado2, Region regionActual){
        Jugada jug = new Jugada(peleo, dado1, dado2, regionActual);
        agregar(jug);
    }
    
    public void agregar(Object objeto){
        Jugada temporal = (Jugada) objeto;
        
        if (cabeza == null){
            insertarInicio(temporal);
        }else{
            Nodo actual = cabeza.getSig();
            Nodo anterior = cabeza;
            Jugada jug = (Jugada) cabeza.getInfo();
            if (jug.getTurno() > temporal.getTurno()){
                insertarInicio(temporal);
            }else{
                Nodo temp = new Nodo();
                temp.setInfo(temporal);
                jug = (Jugada) anterior.getInfo();
                while ((actual != null) && (jug.getTurno() < temporal.getTurno())){
                    actual= actual.getSig();
                    anterior = anterior.getSig();
                }
                temp.setSig(actual);
                anterior.setSig(temp);
            }
        }
    }
    
    public String visualizar(){
        String msn = "";
        Nodo iterador = cabeza;
        while (iterador != null){
            msn += iterador.getInfo().toString() + "\n";
            iterador = iterador.getSig();
        }
        return msn;
    }
    
}
