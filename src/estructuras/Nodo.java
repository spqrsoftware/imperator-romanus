package estructuras;

public class Nodo {
    
    private Object info;
    private Nodo sig;
    
//****************************  Constructores  *******************************//
    
    public Nodo() {
    }   //fin del constructor default
    
//*****************************  Otros m�todos  ******************************//
    public String toString(){
        return "El nodo contiene la siguiente informaci�n: " +
                getInfo().toString();
    }   //fin del m�todo toString
    
//******************************  Sets y gets  *******************************//
    public Object getInfo(){return info;}
    public void setInfo(Object info){this.info = info;}
    
    public Nodo getSig(){return sig;}
    public void setSig(Nodo sig){this.sig = sig;}
    
}   //fin de la claseAbstracta Nodo