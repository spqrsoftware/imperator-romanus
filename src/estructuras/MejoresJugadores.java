package estructuras;

import codigo.Jugador;
import java.io.*;

public class MejoresJugadores {
    
    private Nodo cabeza;
    private final String NOMBRE_ARCHIVO = "Mejores jugadores.mr";
    
    /** Creates a new instance of MejoresJugadores */
    public MejoresJugadores() {
        
    }
    /***************************************** Metodos *****************************************/
    
    public Nodo eliminar(){
        Nodo temp = cabeza;
        cabeza = cabeza.getSig();
        temp.setSig(null);
        return temp;
    }
    
    public void insertarInicio(Jugador j){
        Nodo temp = new Nodo();
        temp.setInfo(j);
        temp.setSig(cabeza);
        cabeza = temp;
    }
    
    public void agregar(Jugador j){
        
        if (cabeza == null){
            insertarInicio(j);
        }else{
            Nodo actual = cabeza.getSig();
            Nodo anterior = cabeza;
            Jugador jug = (Jugador) cabeza.getInfo();
            if (jug.getEmperador().getDominio().size() < j.getEmperador().getDominio().size()){
                insertarInicio(j);
            }else{
                Nodo temp = new Nodo();
                temp.setInfo(j);
                //jug = (Jugador) actual.getInfo();
                while ((actual != null) && (jug.getEmperador().getDominio().size() > j.getEmperador().getDominio().size())){
                    actual= actual.getSig();
                    anterior = anterior.getSig();
                }
                temp.setSig(actual);
                anterior.setSig(temp);
            }
        }
        validarTama�o();
        
    }   //fin del m�todo agregar
    
    private void validarTama�o(){
        Nodo iterador = cabeza;
        int centinela = 0;
        while( iterador != null ){
            if( centinela >= 10 ){
                iterador.setSig(null);
                iterador.setInfo(null);
                iterador = null;
                return;
            }   //fin del if
            iterador = iterador.getSig();
            centinela++;
        }   //fin del while
    }   //fin del m�todo validarTama�o
    
    public String visualizar(){
        String msn = "";
        while (cabeza != null){
            msn += eliminar().getInfo() + "\n";
        }
        return msn;
    }   //fin del m�todo vizualizar
    
//*****************  M�todos para el manejo de flujos  ***********************//
    public void salvar(){
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            fos = new FileOutputStream( NOMBRE_ARCHIVO );
            oos = new ObjectOutputStream( fos );
        }catch( IOException io ){
            System.out.println( "Error de apertura" );
        }   //fin del try & catch
        while( cabeza != null ){
            
            try{
                oos.writeObject( eliminar().getInfo() );
                oos.flush();
            }catch( IOException io ){
                System.out.println( "Error de escritura" );
            }   //fin del try & catch
            
        }   //fin del while
        try{
            oos.close();
        }catch( IOException io ){
            System.out.println( "Error de cierre" );
        }   //fin del try & catch
    }   //fin del m�todo salvar
    
    public void cargar(){
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try{
            fis = new FileInputStream( NOMBRE_ARCHIVO );
            ois = new ObjectInputStream( fis );
        }catch( IOException io ){
            System.out.println( "Error de apertura" );
        }   //fin del try & catch
        Jugador jug = null;
        do{
            try{
                jug = (Jugador) ois.readObject();
                agregar( jug );
            }catch( EOFException eof){
                jug = null;
            }catch(ClassNotFoundException cnf){
                System.out.println( "Error de casting" );
            }catch( IOException io ){
                System.out.println( "Error de carga" );
            }   //fin del try & catch
        }while( jug != null );
        try{
            ois.close();
        }catch( IOException io ){
            System.out.println( "Error de cierre" );
        }   //fin del try & catch
    }   //fin del m�todo cargar
    
}   //fin de la clase MejoresJugadores
