package estructuras;

import codigo.*;
import excepciones.*;
import java.io.*;

public class Cola {
    
    private Nodo primero;
    private Nodo ultimo;
    
//*****************************  Constructores  ******************************//
    
    public Cola() {
    }   //fin del constructor
    
//*****************************  Otros m�todos  ******************************//
    
    /**M�todo de una <code> COLA <code><p>
     *Agrega un nuevo elemento a la cola*/
    public void agregar( Jugador obj ){
        Nodo temp = new Nodo();
        temp.setInfo( obj );
        if( getPrimer() == null || getUltimo() == null ){
            setPrimer(temp);
            setUltimo(temp);
        }else{
            getUltimo().setSig( temp );
            setUltimo(temp);
        }   //fin del if...else
    }   //fin del m�todo agregar
    
    /**
     * M�todo de una <code> COLA <code><p>
     * Quita el primero elemento de la cola
     */
    public void brutallity() throws ListaVaciaException{
        Nodo temp = new Nodo();
        if( getPrimer() == null ){
            throw new ListaVaciaException( "No se pudo borrar" );
        }else{
            temp = getPrimer();
            setPrimer(getPrimer().getSig());
            temp.setInfo( null );
            temp.setSig( null );
            temp = null;
        }   //fin del if...else
    }   //fin del m�todo fatality
    
    /**
     * @deprecated
     * M�todo de una <code> COLA <code><p>
     * Quita el primero elemento de la cola
     *@Desde: que se hizo el m�todo brutality*/
    //@Deprecated
    public String eliminar() throws ListaVaciaException{
        Nodo temp = new Nodo();
        String devolver = "";
        if( getPrimer() == null ){
            throw new ListaVaciaException( "No se pudo borrar" );
        }else{
            temp = getPrimer();
            setPrimer(getPrimer().getSig());
            devolver = "Se ha borrado: " + temp;
            temp.setInfo( null );
            temp.setSig( null );
            temp = null;
            return devolver;
        }   //fin del if...else
    }   //fin del m�todo fatality
    
    public Jugador jugadorActual(){
        return ((Jugador) getPrimer().getInfo() );
    }   //fin del m�todo jugadorActual
    
    public Jugador siguienteJugador(){
        Nodo temp = new Nodo();
        if( getPrimer() != null ){
            temp = getPrimer();
            setPrimer(getPrimer().getSig());
            agregar( ((Jugador)temp.getInfo()) );
            return ((Jugador)getPrimer().getInfo() );
        }   //fin del if
        return null;
    }   //fin del m�todo mover
    
//***************************** Sets y gets  *********************************//
    
    private Nodo getPrimer(){return primero;}
    private void setPrimer(Nodo primer){this.primero = primer;}
    
    private Nodo getUltimo(){return ultimo;}
    private void setUltimo(Nodo ultimo){this.ultimo = ultimo;}
    
    
//********************  M�todos para guardar y cargar  ***********************//
    public void guardar(String nombreArchivo) throws FlujoException, ListaVaciaException{
        ObjectOutputStream salida=null;
        try{
            salida = new ObjectOutputStream(new FileOutputStream(nombreArchivo));
        }catch(IOException e){
            throw new FlujoException("ERROR DE APERTURA");
        }
        Nodo iterador = primero;
        
        while(iterador !=null){
            // Tratar de guardar el elemento en el archivo abierto
            try{
                salida.writeObject(iterador.getInfo());
                salida.flush();
            }catch(IOException e){
                throw new FlujoException("ERROR AL GUARDAR JUGADOR");
            }
            // hacer iterador igual al siguiente
            this.brutallity();
            iterador = getPrimer();
        }   //fin del while
        try{
            salida.close();
        }catch(IOException e){
            throw new FlujoException("ERROR AL CERRAR ARCHIVO");
        }
    }
    
    public void cargar(String nombreArchivo) throws FlujoException{
        ObjectInputStream entrada=null;
        try{
            entrada = new ObjectInputStream(new FileInputStream(new File(nombreArchivo)));
        }catch(IOException e){
            throw new FlujoException("ERROR DE APERTURA");
        }
        Nodo iterador = primero;
        Jugador dato = null;
        do{
            // Tratar de cargar el elemento desde el archivo abierto
            try{
                dato = (Jugador) entrada.readObject();
                this.agregar(dato);// Insertar al final
            }catch(EOFException e){
                dato = null;
            }catch(IOException e){
                throw new FlujoException("ERROR AL CARGAR JUGADOR");
//                System.err.println(e.getMessage());
//                e.printStackTrace();
            }catch (ClassNotFoundException e){
                throw new FlujoException("No Est� Clase Jugador");
            }
            // hacer iterador igual al siguiente
        } while(dato!=null);
    }
    
}   //fin de la clase cola