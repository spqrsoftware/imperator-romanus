package excepciones;

/**Exception que sirve para "tirarse en el momento en el que el usuario le mande datos vacios al sistema*/
public class CampoVacioException extends Exception{
    
    
    public CampoVacioException() {
        super( "Se ha generado una excepción\nPor favor revise los campos de introducción de datos" );
    }   //fin del constructor
    
    public CampoVacioException( String texto ){
        super( "Se ha generado una CampoVacioException\n" + texto );
    }   //fin de constructor
    
}   //fin de la clase CampoVacioException