/*
 * FlujoException.java
 *
 * Created on 5 de marzo de 2006, 02:05 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package excepciones;

/**
 *
 * @author __USER__
 */
public class FlujoException extends Exception{
    
    public FlujoException() {
        super();
    }   //fin del constructor default
    
    public FlujoException(String message) {
        super(message);
    }   //fin de constructor
    
}
