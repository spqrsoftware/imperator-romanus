package excepciones;

public class ListaVaciaException extends Exception{
    
    public ListaVaciaException(){
        super( "Se ha producido una excepci�n.\n La lista se encuentra vac�a" );
    }   //fin del constructor default
    
    public ListaVaciaException( String error ){
        super( "Se ha producido una excepci�n.\n La lista se encuentra vac�a" +
                "\n" + error );
    }   //fin de constructor
    
}   //fin de la clase ListaVaciaException