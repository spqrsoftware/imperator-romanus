package guis;

import codigo.*;
import javax.swing.*;

public class VentanaJugador extends JInternalFrame {
    
    public VentanaJugador() {
        initComponents();
    }
    
    public VentanaJugador( String familia ) {
        initComponents();
        fam = familia;
        actualizarVentana( fam );
    }
    
    // <editor-fold defaultstate="collapsed" desc=" actualizarVentana ">
    private void actualizarVentana( String familia ){
        if( familia.equalsIgnoreCase("Brutii") ){
            label_imperator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_brutii.jpg")) );
            label_imperator.setText( Mesa.jugadorActual().getEmperador().getNombre() );
            label_jugador.setText( Mesa.jugadorActual().getNombre() );
            label_numero_regiones.setText( String.valueOf( Mesa.jugadorActual().getEmperador().getDominio().size()) );
            txt_soldados_ejercito.setText( String.valueOf( Mesa.jugadorActual().getEmperador().getEjercito().size() ) );
            txt_soldados_reserva.setText( String.valueOf( Mesa.jugadorActual().getEmperador().getReserva().getLista_soldados().size() ) );
            numero_vueltas.setText( String.valueOf( Mesa.jugadorActual().getNumero_vueltas() ));
            if( Mesa.calcularRegion(Mesa.getPos_brutii()) instanceof Region ){
                if( Mesa.jugadorActual().getEmperador().equals( Mesa.calcularRegion( Mesa.getPos_brutii()).getEmperador() ) ){
                    region_actual = Mesa.calcularRegion( Mesa.getPos_brutii());
                    txt_region_a_ejercito.setEnabled( true );
                    txt_region_a_reserva.setEnabled( true );
                    txt_ejercito_a_region.setEnabled( true );
                    txt_reserva_a_region.setEnabled( true );
                    btn_cambiar_region.setEnabled( true );
                    txt_soldados_region.setText( String.valueOf( Mesa.calcularRegion( Mesa.getPos_brutii()).getEjercito().size() ) );
                }else{
                    txt_region_a_ejercito.setEnabled( false );
                    txt_region_a_reserva.setEnabled( false );
                    btn_cambiar_region.setEnabled( false );
                    txt_ejercito_a_region.setEnabled( false );
                    txt_reserva_a_region.setEnabled( false );
                    txt_soldados_region.setText( "" );
                }   //fin del if...else
            }   //fin del if
        }   //fin del if
        
        if( familia.equalsIgnoreCase("Scipii") ){
            label_imperator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_scipii.jpg")) );
            label_imperator.setText( Mesa.jugadorActual().getEmperador().getNombre() );
            label_jugador.setText( Mesa.jugadorActual().getNombre() );
            label_numero_regiones.setText( String.valueOf( Mesa.jugadorActual().getEmperador().getDominio().size()) );
            txt_soldados_ejercito.setText( String.valueOf( Mesa.jugadorActual().getEmperador().getEjercito().size() ) );
            txt_soldados_reserva.setText( String.valueOf( Mesa.jugadorActual().getEmperador().getReserva().getLista_soldados().size() ) );
            numero_vueltas.setText( String.valueOf( Mesa.jugadorActual().getNumero_vueltas() ));
            if( Mesa.calcularRegion(Mesa.getPos_scipii()) instanceof Region ){
                if( Mesa.jugadorActual().getEmperador().equals( Mesa.calcularRegion( Mesa.getPos_scipii()).getEmperador() ) ){
                    region_actual = Mesa.calcularRegion( Mesa.getPos_scipii());
                    txt_region_a_ejercito.setEnabled( true );
                    txt_region_a_reserva.setEnabled( true );
                    btn_cambiar_region.setEnabled( true );
                    txt_ejercito_a_region.setEnabled( true );
                    txt_reserva_a_region.setEnabled( true );
                    txt_soldados_region.setText( String.valueOf( Mesa.calcularRegion( Mesa.getPos_scipii()).getEjercito().size() ) );
                }else{
                    txt_region_a_ejercito.setEnabled( false );
                    txt_region_a_reserva.setEnabled( false );
                    btn_cambiar_region.setEnabled( false );
                    txt_ejercito_a_region.setEnabled( false );
                    txt_reserva_a_region.setEnabled( false );
                    txt_soldados_region.setText( "" );
                }   //fin del if...else
            }   //fin del if
        }   //fin del if
        
        if( familia.equalsIgnoreCase("Julii") ){
            label_imperator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_julii.jpg")) );
            label_imperator.setText( Mesa.jugadorActual().getEmperador().getNombre() );
            label_jugador.setText( Mesa.jugadorActual().getNombre() );
            label_numero_regiones.setText( String.valueOf( Mesa.jugadorActual().getEmperador().getDominio().size()) );
            txt_soldados_ejercito.setText( String.valueOf( Mesa.jugadorActual().getEmperador().getEjercito().size() ) );
            txt_soldados_reserva.setText( String.valueOf( Mesa.jugadorActual().getEmperador().getReserva().getLista_soldados().size() ) );
            numero_vueltas.setText( String.valueOf( Mesa.jugadorActual().getNumero_vueltas() ));
            if( Mesa.calcularRegion(Mesa.getPos_julii()) instanceof Region ){
                if( Mesa.jugadorActual().getEmperador().equals( Mesa.calcularRegion( Mesa.getPos_julii()).getEmperador() ) ){
                    region_actual = Mesa.calcularRegion( Mesa.getPos_julii());
                    txt_region_a_ejercito.setEnabled( true );
                    txt_region_a_reserva.setEnabled( true );
                    btn_cambiar_region.setEnabled( true );
                    txt_ejercito_a_region.setEnabled( true );
                    txt_reserva_a_region.setEnabled( true );
                    txt_soldados_region.setText( String.valueOf( Mesa.calcularRegion( Mesa.getPos_julii()).getEjercito().size() ) );
                }else{
                    txt_region_a_ejercito.setEnabled( false );
                    txt_region_a_reserva.setEnabled( false );
                    btn_cambiar_region.setEnabled( false );
                    txt_ejercito_a_region.setEnabled( false );
                    txt_reserva_a_region.setEnabled( false );
                    txt_soldados_region.setText( "" );
                }   //fin del if...else
            }   //fin del if
        }   //fin del if
        
        txt_ejercito_a_region.setText( "" );
        txt_ejercito_a_reserva.setText( "" );
        txt_region_a_ejercito.setText( "" );
        txt_region_a_reserva.setText( "" );
        txt_reserva_a_ejercito.setText( "" );
        txt_reserva_a_region.setText( "" );
        
    }   //fin del m�todo comenzarVentana
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" cambiarEjercito ">
    private void cambiarEjercito(){
        try{
            if( txt_ejercito_a_reserva.getText().trim().length() != 0 ){
                int temp = Integer.parseInt( txt_ejercito_a_reserva.getText() );
                temp -= 1;
                Mesa.jugadorActual().getEmperador().reservarSoldados( temp );
            }   //fin del if
            if( txt_ejercito_a_region.getText().trim().length() != 0 ){
                int temp = Integer.parseInt( txt_ejercito_a_region.getText() );
                temp -= 1;
                Mesa.jugadorActual().getEmperador().dejarSoldadoEjercito(region_actual, temp );
            }   //fin del if
            actualizarVentana(fam);
        }catch( NumberFormatException nfe ){
            JOptionPane.showMessageDialog( null, "Ha digitado mal la cantidad de soldados en los espacios del ej�rcito",
                    nfe.getMessage() , JOptionPane.ERROR_MESSAGE );
            actualizarVentana(fam);
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, e.getMessage(), "Error", JOptionPane.WARNING_MESSAGE );
            actualizarVentana(fam);
        }   //fin del try & catch
    }   //fin del m�todo cambiarEjercito
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" cambiarReserva ">
    private void cambiarReserva(){
        try{
            if( txt_reserva_a_ejercito.getText().trim().length() != 0 ){
                int temp = Integer.parseInt( txt_reserva_a_ejercito.getText() );
                temp -= 1;
                Mesa.jugadorActual().getEmperador().pedirSoldadosAReserva( temp );
            }   //fin del if
            if( txt_reserva_a_region.getText().trim().length() != 0 ){
                int temp = Integer.parseInt( txt_reserva_a_region.getText() );
                temp -= 1;
                Mesa.jugadorActual().getEmperador().dejarSoldadoReserva(region_actual, temp );
            }   //fin del if
            actualizarVentana(fam);
        }catch( NumberFormatException nfe ){
            JOptionPane.showMessageDialog( null, "Ha digitado mal la cantidad de soldados en los espacios de la reseva",
                    nfe.getMessage() , JOptionPane.ERROR_MESSAGE );
            actualizarVentana(fam);
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, e.getMessage(), "Error", JOptionPane.WARNING_MESSAGE );
            actualizarVentana(fam);
        }   //fin del try & catch
    }   //fin del m�todo cambiarEjercito
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" cambiarRegion ">
    private void cambiarRegion(){
        try{
            if( txt_region_a_ejercito.getText().trim().length() != 0 ){
                int temp = Integer.parseInt( txt_region_a_ejercito.getText() );
                temp -= 1;
                Mesa.jugadorActual().getEmperador().retirarSoldadoAEjercito( region_actual, temp );
            }   //fin del if
            if( txt_region_a_reserva.getText().trim().length() != 0 ){
                int temp = Integer.parseInt( txt_region_a_reserva.getText() );
                temp -= 1;
                Mesa.jugadorActual().getEmperador().retirarSoldadoAReserva(region_actual, temp );
            }   //fin del if
            actualizarVentana(fam);
        }catch( NumberFormatException nfe ){
            JOptionPane.showMessageDialog( null, "Ha digitado mal la cantidad de soldados en los espacios de la region",
                    nfe.getMessage() , JOptionPane.ERROR_MESSAGE );
            actualizarVentana(fam);
        }catch( Exception e ){
            JOptionPane.showMessageDialog( null, e.getMessage(), "Error", JOptionPane.WARNING_MESSAGE );
            actualizarVentana(fam);
        }   //fin del try & catch
    }   //fin del m�todo cambiarEjercito
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jPanel1 = new javax.swing.JPanel();
        label_imperator = new javax.swing.JLabel();
        label_jugador = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        label_numero_regiones = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txt_soldados_ejercito = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel10 = new javax.swing.JLabel();
        txt_ejercito_a_reserva = new javax.swing.JTextField();
        txt_ejercito_a_region = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        btn_cambiar_ejercito = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        txt_soldados_reserva = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txt_reserva_a_region = new javax.swing.JTextField();
        txt_reserva_a_ejercito = new javax.swing.JTextField();
        btn_cambiar_reserva = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel17 = new javax.swing.JLabel();
        txt_soldados_region = new javax.swing.JTextField();
        btn_cambiar_region = new javax.swing.JButton();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        txt_region_a_reserva = new javax.swing.JTextField();
        txt_region_a_ejercito = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        numero_vueltas = new javax.swing.JLabel();

        setClosable(true);
        setTitle("Informaci\u00f3n del Jugador");
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        label_imperator.setFont(new java.awt.Font("Californian FB", 1, 14));
        label_imperator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_brutii.jpg")));
        label_imperator.setText("Jugador");
        label_imperator.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        label_imperator.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel1.add(label_imperator, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        label_jugador.setFont(new java.awt.Font("Tahoma", 1, 18));
        label_jugador.setText("Jugador");
        jPanel1.add(label_jugador, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel1.setText("N\u00famero de Regiones:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, -1));

        label_numero_regiones.setFont(new java.awt.Font("Tahoma", 1, 36));
        label_numero_regiones.setText("0");
        jPanel1.add(label_numero_regiones, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 190, 70, 40));

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 0, 20, 280));

        jLabel7.setFont(new java.awt.Font("MS Sans Serif", 1, 12));
        jLabel7.setText("Opciones del jugador");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, -1, -1));

        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 40, -1, -1));

        jLabel9.setText("Soldados en ej\u00e9rcito:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 30, -1, -1));

        txt_soldados_ejercito.setEditable(false);
        jPanel1.add(txt_soldados_ejercito, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 50, 100, -1));

        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 100, 400, 20));

        jLabel10.setText("Dejar soldados del ej\u00e9rcito en:");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 30, -1, -1));

        jPanel1.add(txt_ejercito_a_reserva, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 70, 60, -1));

        txt_ejercito_a_region.setEnabled(false);
        jPanel1.add(txt_ejercito_a_region, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 70, 60, -1));

        jLabel11.setText("Reserva");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 50, -1, 20));

        jLabel12.setText("Regi\u00f3n Actual");
        jPanel1.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 50, -1, 20));

        btn_cambiar_ejercito.setText("Cambiar");
        btn_cambiar_ejercito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cambiar_ejercitoActionPerformed(evt);
            }
        });

        jPanel1.add(btn_cambiar_ejercito, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 73, -1, 20));

        jLabel13.setText("Soldados en reserva:");
        jPanel1.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 110, -1, -1));

        txt_soldados_reserva.setEditable(false);
        jPanel1.add(txt_soldados_reserva, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 130, 100, -1));

        jLabel14.setText("Dejar soldados de la reserva en:");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 110, -1, -1));

        jLabel15.setText("Ej\u00e9rcito");
        jPanel1.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 130, -1, 20));

        jLabel16.setText("Regi\u00f3n Actual");
        jPanel1.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 130, -1, 20));

        txt_reserva_a_region.setEnabled(false);
        jPanel1.add(txt_reserva_a_region, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 150, 60, -1));

        jPanel1.add(txt_reserva_a_ejercito, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 150, 60, -1));

        btn_cambiar_reserva.setText("Cambiar");
        btn_cambiar_reserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cambiar_reservaActionPerformed(evt);
            }
        });

        jPanel1.add(btn_cambiar_reserva, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 153, -1, 20));

        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 180, 400, 20));

        jLabel17.setText("Soldados en la regi\u00f3n actual::");
        jPanel1.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 190, -1, -1));

        txt_soldados_region.setEditable(false);
        jPanel1.add(txt_soldados_region, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 210, 100, -1));

        btn_cambiar_region.setText("Cambiar");
        btn_cambiar_region.setEnabled(false);
        btn_cambiar_region.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cambiar_regionActionPerformed(evt);
            }
        });

        jPanel1.add(btn_cambiar_region, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 240, -1, 20));

        jLabel18.setText("Dejar soldados de la regi\u00f3n actual en:");
        jPanel1.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 190, -1, -1));

        jLabel19.setText("Reserva");
        jPanel1.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 210, -1, 20));

        jLabel20.setText("Ej\u00e9rcito");
        jPanel1.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 210, -1, 20));

        txt_region_a_reserva.setEnabled(false);
        jPanel1.add(txt_region_a_reserva, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 230, 60, -1));

        txt_region_a_ejercito.setEnabled(false);
        jPanel1.add(txt_region_a_ejercito, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 230, 60, -1));

        jLabel2.setText("Vueltas:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, -1, -1));

        numero_vueltas.setText("0");
        jPanel1.add(numero_vueltas, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 240, -1, -1));

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }
    // </editor-fold>//GEN-END:initComponents
    
    private void btn_cambiar_regionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cambiar_regionActionPerformed
        cambiarRegion();
        actualizarVentana(fam);
    }//GEN-LAST:event_btn_cambiar_regionActionPerformed
    
    private void btn_cambiar_reservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cambiar_reservaActionPerformed
        cambiarReserva();
        actualizarVentana(fam);
    }//GEN-LAST:event_btn_cambiar_reservaActionPerformed
    
    private void btn_cambiar_ejercitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cambiar_ejercitoActionPerformed
        cambiarEjercito();
        actualizarVentana(fam);
    }//GEN-LAST:event_btn_cambiar_ejercitoActionPerformed
    
    // <editor-fold defaultstate="collapsed" desc=" Declaraci�n de variables ">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cambiar_ejercito;
    private javax.swing.JButton btn_cambiar_region;
    private javax.swing.JButton btn_cambiar_reserva;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel label_imperator;
    private javax.swing.JLabel label_jugador;
    private javax.swing.JLabel label_numero_regiones;
    private javax.swing.JLabel numero_vueltas;
    private javax.swing.JTextField txt_ejercito_a_region;
    private javax.swing.JTextField txt_ejercito_a_reserva;
    private javax.swing.JTextField txt_region_a_ejercito;
    private javax.swing.JTextField txt_region_a_reserva;
    private javax.swing.JTextField txt_reserva_a_ejercito;
    private javax.swing.JTextField txt_reserva_a_region;
    private javax.swing.JTextField txt_soldados_ejercito;
    private javax.swing.JTextField txt_soldados_region;
    private javax.swing.JTextField txt_soldados_reserva;
    // End of variables declaration//GEN-END:variables
    private Region region_actual = null;
    private String fam = "";
    // </editor-fold>
    
}   //fin de la clase VentanaJugador