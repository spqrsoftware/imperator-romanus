package guis;

import codigo.*;
import excepciones.CampoVacioException;
import java.awt.*;

public class VentanaInicio extends javax.swing.JFrame {
    
    public VentanaInicio() {
        initComponents();
        panel_brutii.setVisible( false );
        panel_scipii.setVisible( false );
        panel_julii.setVisible( false );
    }
    
    // <editor-fold defaultstate="collapsed" desc=" comenzar ">
    private void comenzar(){
        if( btn_Brutii.isSelected() ){
            try{
                uno = new Jugador( txt_jugador1.getText(), brutii );
                Mesa.agregarJugador( uno );
            }catch( CampoVacioException cv ){
                try{
                    uno = new Jugador( "Jugador 1", brutii );
                    Mesa.agregarJugador( uno );
                }catch( CampoVacioException cve ){}
            }   //fin del try & catch
        }   //fin del if
        
        if( btn_Scipii.isSelected() ){
            try{
                dos = new Jugador( txt_jugador2.getText(), scipii );
                Mesa.agregarJugador( dos );
            }catch( CampoVacioException cv ){
                try{
                    dos = new Jugador( "Jugador 2" , scipii );
                    Mesa.agregarJugador( dos );
                }catch( CampoVacioException cve ){}
            }   //fin del try & catch
        }   //fin del if
        
        if( btn_Julii.isSelected() ){
            try{
                tres = new Jugador( txt_jugador3.getText(), julii );
                Mesa.agregarJugador( tres );
            }catch( CampoVacioException cv ){
                try{
                    tres = new Jugador( "Jugador 3" , julii );
                    Mesa.agregarJugador( tres );
                }catch( CampoVacioException cve ){}
            }   //fin del try & catch
        }   //fin del if
        
    }   //fin del m�todo habilitarNombres
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" habilitarComienzo ">
    private void habilitarComienzo(){
        if( (btn_Brutii.isSelected() && btn_Julii.isSelected()) ||
                (btn_Brutii.isSelected() && btn_Scipii.isSelected()) ||
                (btn_Scipii.isSelected() && btn_Julii.isSelected()) ){
            btn_comenzar.setEnabled( true );
        }else{
            btn_comenzar.setEnabled( false );
        }   //fin del if...else
    }   //fin del m�todo habilitarComienzo
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        panel_brutii = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txt_jugador1 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        panel_scipii = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txt_jugador2 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        panel_julii = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        txt_jugador3 = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        brutii_icon = new javax.swing.JLabel();
        scipii_icon = new javax.swing.JLabel();
        julii_icon = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        btn_Scipii = new javax.swing.JToggleButton();
        btn_Julii = new javax.swing.JToggleButton();
        btn_Brutii = new javax.swing.JToggleButton();
        btn_salir = new javax.swing.JButton();
        btn_comenzar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Imperator Romanus SPQR Software");
        setUndecorated(true);
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 0, -1, -1));

        jLabel3.setFont(new java.awt.Font("Californian FB", 1, 72));
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/Imperator Romanu logo.jpg")));
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 10, -1, -1));

        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.X_AXIS));

        jPanel1.setBackground(new java.awt.Color(102, 102, 102));
        jPanel1.setOpaque(false);
        panel_brutii.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel_brutii.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 153, 0), 2, true));
        panel_brutii.setOpaque(false);
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Jugador uno:");
        panel_brutii.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        panel_brutii.add(txt_jugador1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 160, -1));

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Imperator:");
        panel_brutii.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, -1));

        jLabel7.setFont(new java.awt.Font("Californian FB", 1, 14));
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_brutii.jpg")));
        jLabel7.setText("Tiberius Brutus");
        panel_brutii.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 220, -1));

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Familia:");
        panel_brutii.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, -1, -1));

        jLabel9.setFont(new java.awt.Font("Californian FB", 1, 14));
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii.jpg")));
        jLabel9.setText("Brutii");
        panel_brutii.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 150, -1));

        jPanel1.add(panel_brutii);

        panel_scipii.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel_scipii.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 153), 2, true));
        panel_scipii.setOpaque(false);
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Jugador dos:");
        panel_scipii.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        panel_scipii.add(txt_jugador2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 160, -1));

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Imperator:");
        panel_scipii.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, -1));

        jLabel12.setFont(new java.awt.Font("Californian FB", 1, 14));
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_scipii.jpg")));
        jLabel12.setText("Cornelius Scipio");
        panel_scipii.add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 220, -1));

        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Familia:");
        panel_scipii.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, -1, -1));

        jLabel14.setFont(new java.awt.Font("Californian FB", 1, 14));
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii.jpg")));
        jLabel14.setText("Scipii");
        panel_scipii.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 150, -1));

        jPanel1.add(panel_scipii);

        panel_julii.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel_julii.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 0, 0), 2, true));
        panel_julii.setOpaque(false);
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Jugador tres:");
        panel_julii.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        panel_julii.add(txt_jugador3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 160, -1));

        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Imperator:");
        panel_julii.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, -1));

        jLabel17.setFont(new java.awt.Font("Californian FB", 1, 14));
        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_julii.jpg")));
        jLabel17.setText("Flavius Julii");
        panel_julii.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 220, -1));

        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Familia:");
        panel_julii.add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, -1, -1));

        jLabel19.setFont(new java.awt.Font("Californian FB", 1, 14));
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii.jpg")));
        jLabel19.setText("Julii");
        panel_julii.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 150, -1));

        jPanel1.add(panel_julii);

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 370, 980, 370));

        brutii_icon.setFont(new java.awt.Font("Tahoma", 0, 12));
        brutii_icon.setForeground(new java.awt.Color(255, 255, 255));
        brutii_icon.setText("Brutii");
        brutii_icon.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        brutii_icon.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(brutii_icon, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 270, -1, -1));

        scipii_icon.setFont(new java.awt.Font("Tahoma", 0, 12));
        scipii_icon.setForeground(new java.awt.Color(255, 255, 255));
        scipii_icon.setText("Scipii");
        scipii_icon.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        scipii_icon.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(scipii_icon, new org.netbeans.lib.awtextra.AbsoluteConstraints(486, 270, 30, -1));

        julii_icon.setFont(new java.awt.Font("Tahoma", 0, 12));
        julii_icon.setForeground(new java.awt.Color(255, 255, 255));
        julii_icon.setText("Julii");
        julii_icon.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        julii_icon.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        getContentPane().add(julii_icon, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 270, -1, -1));

        jLabel22.setFont(new java.awt.Font("Californian FB", 1, 14));
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Seleccione dos o tres familias");
        getContentPane().add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 180, 170, -1));

        btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_grey.jpg")));
        btn_Scipii.setMnemonic('S');
        btn_Scipii.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ScipiiActionPerformed(evt);
            }
        });
        btn_Scipii.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_ScipiiMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_ScipiiMouseExited(evt);
            }
        });

        getContentPane().add(btn_Scipii, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 210, 60, 50));

        btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_grey.jpg")));
        btn_Julii.setMnemonic('J');
        btn_Julii.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_JuliiActionPerformed(evt);
            }
        });
        btn_Julii.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_JuliiMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_JuliiMouseExited(evt);
            }
        });

        getContentPane().add(btn_Julii, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 210, 60, 50));

        btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_grey.jpg")));
        btn_Brutii.setMnemonic('B');
        btn_Brutii.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_BrutiiActionPerformed(evt);
            }
        });
        btn_Brutii.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_BrutiiMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_BrutiiMouseExited(evt);
            }
        });

        getContentPane().add(btn_Brutii, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 210, 60, 50));

        btn_salir.setFont(new java.awt.Font("Californian FB", 1, 18));
        btn_salir.setForeground(new java.awt.Color(255, 255, 255));
        btn_salir.setMnemonic('r');
        btn_salir.setText("Salir");
        btn_salir.setToolTipText("Sale del juego");
        btn_salir.setOpaque(false);
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });
        btn_salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_salirMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_salirMouseExited(evt);
            }
        });

        getContentPane().add(btn_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 210, 140, 60));

        btn_comenzar.setForeground(new java.awt.Color(255, 255, 255));
        btn_comenzar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_senate_roll.jpg")));
        btn_comenzar.setMnemonic('c');
        btn_comenzar.setText("Comenzar");
        btn_comenzar.setToolTipText("Comienza la partida");
        btn_comenzar.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_senate_grey.jpg")));
        btn_comenzar.setEnabled(false);
        btn_comenzar.setOpaque(false);
        btn_comenzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_comenzarActionPerformed(evt);
            }
        });

        getContentPane().add(btn_comenzar, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 210, -1, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/loading_screen_8.jpg")));
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }
    // </editor-fold>//GEN-END:initComponents
    
    //TODO modificar este metodo
    private void btn_comenzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_comenzarActionPerformed
        comenzar();
        dispose();
        vm = new VentanaMesa();
        
        // Get the size of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        
        // Determine the new location of the window
        int w = vm.getSize().width;
        int h = vm.getSize().height;
        int x = (dim.width-w)/2;
        int y = (dim.height-h)/2;
        
        // Move the window
        vm.setLocation(x, y);
        
        vm.setVisible( true );
    }//GEN-LAST:event_btn_comenzarActionPerformed
    
    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        System.exit( 0 );
    }//GEN-LAST:event_btn_salirActionPerformed
    
    private void btn_salirMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_salirMouseExited
        btn_salir.setForeground( Color.WHITE );
    }//GEN-LAST:event_btn_salirMouseExited
    
    private void btn_salirMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_salirMouseEntered
        btn_salir.setForeground( Color.YELLOW );
    }//GEN-LAST:event_btn_salirMouseEntered
    
//********************  M�todos para que se vea bonito  **********************//
    // <editor-fold defaultstate="collapsed" desc=" btn_BrutiiActionPerformed ">
    private void btn_BrutiiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_BrutiiActionPerformed
        if( btn_Brutii.isSelected() ){
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_roll.jpg")));
            panel_brutii.setVisible( true );
            habilitarComienzo();
            brutii = new Imperator( "Tiberius Brutus", "Brutii" );
            for( int i = 0; i < 50; i++ ){
                brutii.getReserva().getLista_soldados().add( new Soldado() );
            }   //fin del for
        }else{
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_grey.jpg")));
            panel_brutii.setVisible( false );
            txt_jugador1.setText( "" );
            habilitarComienzo();
            brutii = null;
        }   //fin del if...else
    }//GEN-LAST:event_btn_BrutiiActionPerformed
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" btn_JuliiActionPerformed ">
    private void btn_JuliiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_JuliiActionPerformed
        if( btn_Julii.isSelected() ){
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_roll.jpg")));
            panel_julii.setVisible( true );
            habilitarComienzo();
            julii = new Imperator( "Flavius Julius", "Julii" );
            for( int i = 0; i < 50; i++ ){
                julii.getReserva().getLista_soldados().add( new Soldado() );
            }   //fin del for
        }else{
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_grey.jpg")));
            panel_julii.setVisible( false );
            txt_jugador3.setText( "" );
            habilitarComienzo();
            julii = null;
        }   //fin del if...else
    }//GEN-LAST:event_btn_JuliiActionPerformed
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" btn_ScipiiActionPerformed ">
    private void btn_ScipiiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ScipiiActionPerformed
        if( btn_Scipii.isSelected() ){
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_roll.jpg")));
            panel_scipii.setVisible( true );
            habilitarComienzo();
            scipii = new Imperator( "Cornelius Scipio", "Scipii" );
            for( int i = 0; i < 50; i++ ){
                scipii.getReserva().getLista_soldados().add( new Soldado() );
            }   //fin del for
        }else{
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_grey.jpg")));
            panel_scipii.setVisible( false );
            txt_jugador2.setText( "" );
            habilitarComienzo();
            scipii = null;
        }   //fin del if...else
    }//GEN-LAST:event_btn_ScipiiActionPerformed
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" btn_JuliiMouseExited ">
    private void btn_JuliiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_JuliiMouseExited
        if( btn_Julii.isSelected() ){
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_roll.jpg")));
            panel_julii.setVisible( true );
        }else{
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_grey.jpg")));
            panel_julii.setVisible( false );
        }   //fin del if...else
    }//GEN-LAST:event_btn_JuliiMouseExited
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" btn_JuliiMouseEntered ">
    private void btn_JuliiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_JuliiMouseEntered
        if( btn_Julii.isSelected() ){
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_roll.jpg")));
            panel_julii.setVisible( true );
        }else{
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii.jpg")));
            panel_julii.setVisible( false );
        }   //fin del if...else
    }//GEN-LAST:event_btn_JuliiMouseEntered
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" btn_ScipiiMouseExited ">
    private void btn_ScipiiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_ScipiiMouseExited
        if( btn_Scipii.isSelected() ){
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_roll.jpg")));
            panel_scipii.setVisible( true );
        }else{
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_grey.jpg")));
            panel_scipii.setVisible( false );
        }   //fin del if...else
    }//GEN-LAST:event_btn_ScipiiMouseExited
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" btn_ScipiiMouseEntered ">
    private void btn_ScipiiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_ScipiiMouseEntered
        if( btn_Scipii.isSelected() ){
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_roll.jpg")));
            panel_scipii.setVisible( true );
        }else{
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii.jpg")));
            panel_scipii.setVisible( false );
        }   //fin del if...else
    }//GEN-LAST:event_btn_ScipiiMouseEntered
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" btn_BrutiiMouseExited ">
    private void btn_BrutiiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_BrutiiMouseExited
        if( btn_Brutii.isSelected() ){
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_roll.jpg")));
            panel_brutii.setVisible( true );
        }else{
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_grey.jpg")));
            panel_brutii.setVisible( false );
        }   //fin del if...else
    }//GEN-LAST:event_btn_BrutiiMouseExited
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" btn_BrutiiMouseEntered ">
    private void btn_BrutiiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_BrutiiMouseEntered
        if( btn_Brutii.isSelected() ){
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_roll.jpg")));
            panel_brutii.setVisible( true );
        }else{
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii.jpg")));
            panel_brutii.setVisible( false );
        }   //fin del if...else
    }//GEN-LAST:event_btn_BrutiiMouseEntered
    // </editor-fold>
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel brutii_icon;
    private javax.swing.JToggleButton btn_Brutii;
    private javax.swing.JToggleButton btn_Julii;
    private javax.swing.JToggleButton btn_Scipii;
    private javax.swing.JButton btn_comenzar;
    private javax.swing.JButton btn_salir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel julii_icon;
    private javax.swing.JPanel panel_brutii;
    private javax.swing.JPanel panel_julii;
    private javax.swing.JPanel panel_scipii;
    private javax.swing.JLabel scipii_icon;
    private javax.swing.JTextField txt_jugador1;
    private javax.swing.JTextField txt_jugador2;
    private javax.swing.JTextField txt_jugador3;
    // End of variables declaration//GEN-END:variables
    private Jugador uno;
    private Jugador dos;
    private Jugador tres;
    private Imperator brutii;
    private Imperator scipii;
    private Imperator julii;
    private VentanaMesa vm;
}   //fin de la clase VentanaInicio