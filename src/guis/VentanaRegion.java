package guis;

import codigo.Region;

public class VentanaRegion extends javax.swing.JInternalFrame {
    
    /** Creates new form VentanaRegion */
    public VentanaRegion( Region r ) {
        initComponents();
        actualizarPantalla( r );
    }
    
    private void actualizarPantalla( Region r ){
        if( r.getEmperador() != null ){
            nombre_region.setText( r.getNombre() );
            ciudad.setText( r.getCiudad() );
            produccion_de_soldados.setText( String.valueOf( r.getSoldadosGenerados()) );
            if( "Brutii".equals(r.getEmperador().getFamilia()) ){
                imperator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_brutii.jpg")) );
            }   //fin del if
            if( "Scipii".equals(r.getEmperador().getFamilia()) ){
                imperator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_scipii.jpg")) );
            }   //fin del if
            if( "Julii".equals(r.getEmperador().getFamilia()) ){
                imperator.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_julii.jpg")) );
            }   //fin del if
            imperator.setText( r.getEmperador().getNombre() );
            soldados.setText( String.valueOf( r.getEjercito().size() ) );
        } else{
            nombre_region.setText( r.getNombre() );
            ciudad.setText( r.getCiudad() );
            produccion_de_soldados.setText( String.valueOf( r.getSoldadosGenerados()) );
        }   //fin del if...else
    }   //fin del m�todo actualizarPantalla
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jPanel1 = new javax.swing.JPanel();
        nombre_region = new javax.swing.JLabel();
        ciudad = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        imperator = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        soldados = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        produccion_de_soldados = new javax.swing.JLabel();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        setClosable(true);
        setTitle("Ventana de la Regi\u00f3n");
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        nombre_region.setFont(new java.awt.Font("Californian FB", 1, 24));
        nombre_region.setText("Nombre de la regi\u00f3n");
        jPanel1.add(nombre_region, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        ciudad.setFont(new java.awt.Font("Californian FB", 1, 18));
        ciudad.setText("Ciudad");
        jPanel1.add(ciudad, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        jLabel1.setText("Imperator:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, -1));

        imperator.setFont(new java.awt.Font("Tahoma", 1, 11));
        imperator.setText("-Sin conquistar-");
        jPanel1.add(imperator, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, -1));

        jLabel2.setText("Soldados posicionados:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 140, -1));

        soldados.setFont(new java.awt.Font("Tahoma", 1, 11));
        soldados.setText("-Sin conquistar-");
        jPanel1.add(soldados, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 220, -1, -1));

        jLabel3.setText("Soldados que produce:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 70, -1, -1));

        produccion_de_soldados.setFont(new java.awt.Font("MS Sans Serif", 1, 36));
        produccion_de_soldados.setText("0");
        jPanel1.add(produccion_de_soldados, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 60, 50, 40));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 410, 270));

        pack();
    }
    // </editor-fold>//GEN-END:initComponents
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ciudad;
    private javax.swing.JLabel imperator;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel nombre_region;
    private javax.swing.JLabel produccion_de_soldados;
    private javax.swing.JLabel soldados;
    // End of variables declaration//GEN-END:variables
    
}
