package guis;

import codigo.*;
import codigo.cartas.*;
import codigo.casillas.*;
import excepciones.ListaVaciaException;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class VentanaMesa extends javax.swing.JFrame {
    
    public VentanaMesa() {
        initComponents();
        label_jugador.setVisible( false );
        inicioTurno();
        iconoCarta = new ImageIcon(getClass().getResource("/imagenes/iconos/evil_frame 50.jpg"));
    }
    
    private void producirSoldados(){
        for( int i = 0; i < 36; i++ ){
            if( Mesa.calcularRegion(i) instanceof Region ){
                Mesa.calcularRegion(i).producirSoldados();
            }   //fin del if
        }   //fin del for
    }   //fin del m�todo producirSoldados
    
    // <editor-fold defaultstate="collapsed" desc=" actualizarBoton ">
    private void actualizarBoton(){
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase( "Brutii" )){
            if( Mesa.calcularRegion(Mesa.getPos_brutii()) instanceof Region ){
                btn_pelear.setEnabled( true );
                if( Mesa.jugadorActual().getEmperador().equals( Mesa.calcularRegion(Mesa.getPos_brutii()).getEmperador() )){
                    btn_pelear.setEnabled( false );
                }
            }else{
                btn_pelear.setEnabled( false );
            }   //fin del if...else
        }   //fin del if..else
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase( "Scipii" )){
            if( Mesa.calcularRegion(Mesa.getPos_scipii()) instanceof Region ){
                btn_pelear.setEnabled( true );
                if( Mesa.jugadorActual().getEmperador().equals( Mesa.calcularRegion(Mesa.getPos_scipii()).getEmperador() )){
                    btn_pelear.setEnabled( false );
                }
            }else{
                btn_pelear.setEnabled( false );
            }   //fin del if...else
        }   //fin del if...else
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase( "Julii" )){
            if( Mesa.calcularRegion(Mesa.getPos_julii()) instanceof Region ){
                btn_pelear.setEnabled( true );
                if( Mesa.jugadorActual().getEmperador().equals( Mesa.calcularRegion(Mesa.getPos_julii()).getEmperador() )){
                    btn_pelear.setEnabled( false );
                }
            }else{
                btn_pelear.setEnabled( false );
            }   //fin del if...else
        }   //fin del if...else
    }   //fin del m�todo actualizarBoton
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" pintarBoton ">
    private void pintarBoton(){
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase("Brutii") ){
            btn_opciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii.jpg")) );
        }   //fin del if
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase("Scipii") ){
            btn_opciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii.jpg")) );
        }   //fin del if
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase("Julii") ){
            btn_opciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii.jpg")) );
        }   //fin del if
    }   //fin del m�todo pintarBoton
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" inicioTurno ">
    private void inicioTurno(){
        Mesa.nuevaJugada();
        btn_tirar_dados.setEnabled( true );
        pintarBoton();
        analizarVictoria();
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase("Brutii") ){
            label_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_brutii.jpg")) );
            label_jugador.setText( Mesa.jugadorActual().getNombre() );
            panel_cara_jugador.add( label_jugador );
            label_jugador.validate();
            label_jugador.setVisible( true );
            escritorio.setBackground( VERDE );
            actualizarBoton();
            Mesa.jugadorActual().getEmperador().aumentarTurno();
        }   //fin del if
        
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase("Scipii") ){
            label_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_scipii.jpg")) );
            label_jugador.setText( Mesa.jugadorActual().getNombre() );
            panel_cara_jugador.add( label_jugador );
            label_jugador.validate();
            label_jugador.setVisible( true );
            escritorio.setBackground( AZUL );
            actualizarBoton();
            Mesa.jugadorActual().getEmperador().aumentarTurno();
        }   //fin del if
        
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase("Julii") ){
            label_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_julii.jpg")) );
            label_jugador.setText( Mesa.jugadorActual().getNombre() );
            panel_cara_jugador.add( label_jugador );
            label_jugador.validate();
            label_jugador.setVisible( true );
            escritorio.setBackground( ROJO );
            actualizarBoton();
            Mesa.jugadorActual().getEmperador().aumentarTurno();
        }   //fin del if
        
    }   //fin del inicioTurno
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" moverFicha ">
    private void moverFicha( int n, String familia ){
        if("Brutii".equalsIgnoreCase(familia)){
            int pos_temp = Mesa.getPos_brutii();
            ficha_brutii.setVisible(false);
            encontrarFicha( pos_temp ).remove( ficha_brutii );
            pos_temp += n;
            pos_temp = validaVuelta(pos_temp);
            pos_temp = mandarAColiseo(pos_temp);
            if (pos_temp != 24){
                pos_temp = validarCarta(pos_temp);
            }
            encontrarFicha( pos_temp ).add( ficha_brutii );
            encontrarFicha( pos_temp ).validate();
            ficha_brutii.setVisible( true );
            Mesa.setPos_brutii( pos_temp );
        }   //fin del if
        
        if("Scipii".equalsIgnoreCase(familia)){
            int pos_temp = Mesa.getPos_scipii();
            ficha_scipii.setVisible(false);
            encontrarFicha( pos_temp ).remove( ficha_scipii );
            pos_temp += n;
            pos_temp = validaVuelta(pos_temp);
            pos_temp = mandarAColiseo(pos_temp);
            pos_temp = validarCarta(pos_temp);
            encontrarFicha( pos_temp ).add( ficha_scipii );
            encontrarFicha( pos_temp ).validate();
            ficha_scipii.setVisible( true );
            Mesa.setPos_scipii( pos_temp );
        }   //fin del if
        
        if("Julii".equalsIgnoreCase(familia)){
            int pos_temp = Mesa.getPos_julii();
            ficha_julii.setVisible(false);
            encontrarFicha( pos_temp ).remove( ficha_julii );
            pos_temp += n;
            pos_temp = validaVuelta(pos_temp);
            pos_temp = mandarAColiseo(pos_temp);
            pos_temp = validarCarta(pos_temp);
            JPanel panel = encontrarFicha( pos_temp );
            panel.add( ficha_julii );
            panel.validate();
            ficha_julii.setVisible( true );
            Mesa.setPos_julii( pos_temp );
        }   //fin del if
    }   //fin del m�todo moverFicha
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" validaVuelta ">
    private int validaVuelta(int posicion){
        if (posicion > 36){
            Mesa.jugadorActual().agregarVuelta();
            posicion -= 37;
        }
        return posicion;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" validarCarta ">
    private int validarCarta(int posicion){
        if ((posicion == 4) || (posicion == 28)){
            try{
                Carta carta = (Carta) Mesa.getCartas().pop();
                JOptionPane.showMessageDialog( null, carta.toString(), "Mandatvm",
                        JOptionPane.INFORMATION_MESSAGE, iconoCarta );
                posicion = carta.efecto(posicion);
                Casilla [] casillas = Mesa.getCasillas();
                Casilla temp = casillas [posicion];
                if (temp instanceof Coliseum){
                    ((Coliseum) temp).efecto();
                    finDelTurno();
                }
            }catch (ListaVaciaException e){
                JOptionPane.showMessageDialog( null, "No hay m�s cartas disponibles" , "Mandatvm",
                        JOptionPane.INFORMATION_MESSAGE, iconoCarta );
            }
        }
        return posicion;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" encontrarFicha ">
    private JPanel encontrarFicha( int pos ){
        switch( pos ){
            case 0: return panel_senatus;   case 1: return panel1;
            case 2: return panel2;   case 3: return panel3;
            case 4: return panel_carta1;   case 5: return panel4;
            case 6: return panel5;   case 7: return panel6;
            case 8: return panel7;   case 9: return panel8;
            case 10: return panel9;  case 11: return panel_al_coliseo;
            case 12: return panel10;  case 13: return panel11;
            case 14: return panel12;  case 15: return panel13;
            case 16: return panel14;  case 17: return panel15;
            case 18: return panel16;  case 19: return panel_ba�os;
            case 20: return panel17;  case 21: return panel18;
            case 22: return panel19;  case 23: return panel20;
            case 24: return panel_coliseum;  case 25: return panel21;
            case 26: return panel22;  case 27: return panel23;
            case 28: return panel_carta2;  case 29: return panel24;
            case 30: return panel25;  case 31: return panel26;
            case 32: return panel27;  case 33: return panel28;
            case 34: return panel29;  case 35: return panel30;
            case 36: return panel31;
        }   //fin del switch
        return null;
    }   //fin del m�todo encontrarFicha
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" mandarAColiseo ">
    private int mandarAColiseo(int posicion){
        if ((posicion == 11) || (posicion == 24)){
            posicion = 24;
            JOptionPane.showMessageDialog( null, "Senatvs: \" El senado necesita que el Imperator asista a los juegos\n" +
                    "Debe ir al Coliseo\" ", "Mandatvm", JOptionPane.WARNING_MESSAGE );
            Casilla [] casillas = Mesa.getCasillas();
            Casilla temp = casillas [posicion];
            if (temp instanceof Coliseum){
                ((Coliseum) temp).efecto();
            }
            finDelTurno();
        }
        return posicion;
    }  //fin del m�todo mandar a coliseo
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" M�todos mostrar ventanas internas ">
    // <editor-fold defaultstate="collapsed" desc=" mostrarVentanaMesa ">
    private void mostrarVentanaJugador(){
        pintarCasilla();
        JInternalFrame frame = new VentanaJugador(Mesa.jugadorActual().getEmperador().getFamilia());
        escritorio.add(frame, javax.swing.JLayeredPane.DEFAULT_LAYER);
        frame.validate();
        frame.setVisible(true);
    }   //fin del m�todo mostrarVentanaJugador
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" mostrarVentanaRegi�n ">
    private void mostrarVentanaRegi�n( Region r){
        pintarCasilla();
        JInternalFrame frame = new VentanaRegion( r );
        escritorio.add( frame,javax.swing.JLayeredPane.DEFAULT_LAYER);
        frame.validate();
        frame.setVisible(true);
    }   //fin del m�todo mostrarVentanaRegi�n
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" mostrarVentanaGuerra ">
    private void mostrarVentanaGuerra( Imperator imp, Region r ){
        pintarCasilla();
        JInternalFrame frame = new VentanaGuerra(imp, r);
        escritorio.add(frame, javax.swing.JLayeredPane.DEFAULT_LAYER);
        frame.validate();
        frame.setVisible(true);
    }   //fin del m�todo mostrarVentanaJugador
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" mostrarVentanaVictoria ">
    private void mostrarVentanaVictoria( Imperator imp ){
        JFrame frame = new VentanaVic( imp );
        
        // Get the size of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        
        // Determine the new location of the window
        int w = frame.getSize().width;
        int h = frame.getSize().height;
        int x = (dim.width-w)/2;
        int y = (dim.height-h)/2;
        
        // Move the window
        frame.setLocation(x, y);
        
        frame.setVisible(true);
        this.dispose();
    }   //fin del m�todo mostrarVentanaJugador
    // </editor-fold>
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" M�todos para pintar las regiones ">
    private Color escogerColor( Region r){
        if( r.getEmperador() != null){
            if( ("Brutii").equalsIgnoreCase(r.getEmperador().getFamilia()) ){
                return VERDE_CASILLA;
            }   //fin del if
            if( ("Scipii").equalsIgnoreCase(r.getEmperador().getFamilia())){
                return AZUL_CASILLA;
            }   //fin del if
            if( ("Julii").equalsIgnoreCase(r.getEmperador().getFamilia())){
                return ROJO_CASILLA;
            }   //fin del if
        }else{
            return Color.LIGHT_GRAY;
        }   //fin del if...else
        return Color.LIGHT_GRAY;
    }   //fin del m�todo pintarCasilla
    
    private void pintarCasilla( ){
        panel1.setBackground( escogerColor( (Region) encontrarRegion(panel1) ) );
        panel2.setBackground( escogerColor( (Region) encontrarRegion(panel2) ) );
        panel3.setBackground( escogerColor( (Region) encontrarRegion(panel3) ) );
        panel4.setBackground( escogerColor( (Region) encontrarRegion(panel4) ) );
        panel5.setBackground( escogerColor( (Region) encontrarRegion(panel5) ) );
        panel6.setBackground( escogerColor( (Region) encontrarRegion(panel6) ) );
        panel7.setBackground( escogerColor( (Region) encontrarRegion(panel7) ) );
        panel8.setBackground( escogerColor( (Region) encontrarRegion(panel8) ) );
        panel9.setBackground( escogerColor( (Region) encontrarRegion(panel9) ) );
        panel10.setBackground( escogerColor( (Region) encontrarRegion(panel10) ) );
        panel11.setBackground( escogerColor( (Region) encontrarRegion(panel11) ) );
        panel12.setBackground( escogerColor( (Region) encontrarRegion(panel12) ) );
        panel13.setBackground( escogerColor( (Region) encontrarRegion(panel13) ) );
        panel14.setBackground( escogerColor( (Region) encontrarRegion(panel14) ) );
        panel15.setBackground( escogerColor( (Region) encontrarRegion(panel15) ) );
        panel16.setBackground( escogerColor( (Region) encontrarRegion(panel16) ) );
        panel17.setBackground( escogerColor( (Region) encontrarRegion(panel17) ) );
        panel18.setBackground( escogerColor( (Region) encontrarRegion(panel18) ) );
        panel19.setBackground( escogerColor( (Region) encontrarRegion(panel19) ) );
        panel20.setBackground( escogerColor( (Region) encontrarRegion(panel20) ) );
        panel21.setBackground( escogerColor( (Region) encontrarRegion(panel21) ) );
        panel22.setBackground( escogerColor( (Region) encontrarRegion(panel22) ) );
        panel23.setBackground( escogerColor( (Region) encontrarRegion(panel23) ) );
        panel24.setBackground( escogerColor( (Region) encontrarRegion(panel24) ) );
        panel25.setBackground( escogerColor( (Region) encontrarRegion(panel25) ) );
        panel26.setBackground( escogerColor( (Region) encontrarRegion(panel26) ) );
        panel27.setBackground( escogerColor( (Region) encontrarRegion(panel27) ) );
        panel28.setBackground( escogerColor( (Region) encontrarRegion(panel28) ) );
        panel29.setBackground( escogerColor( (Region) encontrarRegion(panel29) ) );
        panel30.setBackground( escogerColor( (Region) encontrarRegion(panel30) ) );
        panel31.setBackground( escogerColor( (Region) encontrarRegion(panel31) ) );
        
    }
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" encontrarRegion ">
    /** Agarra el panel que recibe por parametro y retorna una Region
     */
    private Casilla encontrarRegion(JPanel panel){
        Region r = null;
        Component [] componentes = panel.getComponents();
        boolean seguir = true;
        for (int i = 0; seguir && (i < componentes.length); i++){
            if (componentes[i] instanceof JLabel){
                JLabel label = (JLabel) componentes[i];
                if (label.getIcon() != null){
                    String nombre = label.getText();
                    Casilla [] casillas = Mesa.getCasillas();
                    for (int j = 0; j < casillas.length ; j++){
                        Casilla temp = casillas[j];
                        if (temp.getNombre().equals(nombre)){
                            if( temp instanceof Region ){
                                r = ( Region ) temp;
                                seguir = false;
                            }else{
                                seguir = false;
                                return temp;
                            }   //fin del if...else
                        }   //fin de if
                    }   //fin de for
                }   //fin de if
            }   //fin de if
        }   //fin de for
        return r;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" mostrarRegion ">
    private void mostrarRegion( MouseEvent evt ){
        JPanel panel = (JPanel) evt.getSource();
        Casilla cas = encontrarRegion(panel);
        if (cas instanceof Region){
            Region reg = (Region) cas;
            mostrarVentanaRegi�n(reg);
        }   //fin de if
    }   //fin del m�todo mostrarRegion
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" finDelTurno ">
    private void finDelTurno(){
        pintarCasilla();
        int pos = 0;
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase( "Brutii")){
            numero_turno_brutii++;
            pos = Mesa.getPos_brutii();
        }   //final del if
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase( "Julii")){
            numero_turno_julii++;
            pos = Mesa.getPos_julii();
        }   //final del if
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase( "Scipii")){
            numero_turno_scipii++;
            pos = Mesa.getPos_scipii();
        }   //final del if
        Mesa.getJug().setRegionActual(this.encontrarRegion(this.encontrarFicha(pos)));
        Mesa.agregarJugada();
        Mesa.getJugadores().siguienteJugador();
        producirSoldados();
        inicioTurno();
    }   //fin del m�todo finDelTurno
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" analizarVistoria ">
    private void analizarVictoria(){
        if( Mesa.jugadorActual().getNumero_vueltas() > 3 ){
            mostrarVentanaVictoria( Mesa.jugadorActual().getEmperador() );
        }   //fin del if
    }   //fin del m�todo analizarVictoria
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jButton1 = new javax.swing.JButton();
        escritorio = new javax.swing.JDesktopPane();
        panel_senatus = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        ficha_scipii = new javax.swing.JLabel();
        ficha_brutii = new javax.swing.JLabel();
        ficha_julii = new javax.swing.JLabel();
        panel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        panel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        panel4 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        panel_carta1 = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        panel3 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        panel5 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        panel6 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        panel7 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        panel8 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        panel9 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        panel_al_coliseo = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        panel10 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        panel11 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        panel12 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        panel13 = new javax.swing.JPanel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        panel14 = new javax.swing.JPanel();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        panel15 = new javax.swing.JPanel();
        jLabel36 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        panel16 = new javax.swing.JPanel();
        jLabel38 = new javax.swing.JLabel();
        jLabel39 = new javax.swing.JLabel();
        panel_ba�os = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        panel17 = new javax.swing.JPanel();
        jLabel42 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();
        panel18 = new javax.swing.JPanel();
        jLabel44 = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        panel19 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        panel_coliseum = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        panel20 = new javax.swing.JPanel();
        jLabel49 = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        panel21 = new javax.swing.JPanel();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        panel22 = new javax.swing.JPanel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        panel23 = new javax.swing.JPanel();
        jLabel57 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        panel_carta2 = new javax.swing.JPanel();
        jLabel40 = new javax.swing.JLabel();
        panel26 = new javax.swing.JPanel();
        jLabel65 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        panel25 = new javax.swing.JPanel();
        jLabel63 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        panel24 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        panel27 = new javax.swing.JPanel();
        jLabel67 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        panel28 = new javax.swing.JPanel();
        jLabel69 = new javax.swing.JLabel();
        jLabel68 = new javax.swing.JLabel();
        panel29 = new javax.swing.JPanel();
        jLabel71 = new javax.swing.JLabel();
        jLabel70 = new javax.swing.JLabel();
        panel30 = new javax.swing.JPanel();
        jLabel73 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        panel31 = new javax.swing.JPanel();
        jLabel75 = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        panel_cara_jugador = new javax.swing.JPanel();
        label_jugador = new javax.swing.JLabel();
        jPanel40 = new javax.swing.JPanel();
        jLabel76 = new javax.swing.JLabel();
        panel_opciones = new javax.swing.JPanel();
        btn_salir = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        btn_tirar_dados = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        dado_uno = new javax.swing.JLabel();
        dado_dos = new javax.swing.JLabel();
        btn_pelear = new javax.swing.JButton();
        btn_fin_turno = new javax.swing.JButton();
        btn_opciones = new javax.swing.JButton();
        jLabel41 = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Imperator Romanus");
        setResizable(false);
        setUndecorated(true);
        escritorio.setBackground(new java.awt.Color(153, 0, 204));
        panel_senatus.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        panel_senatus.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_senate.jpg")));
        jLabel1.setFocusable(false);
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel_senatus.add(jLabel1);

        ficha_scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol24_scipii.jpg")));
        panel_senatus.add(ficha_scipii);

        ficha_brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol24_brutii.jpg")));
        panel_senatus.add(ficha_brutii);

        ficha_julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol24_julii.jpg")));
        panel_senatus.add(ficha_julii);

        panel_senatus.setBounds(20, 20, 110, 100);
        escritorio.add(panel_senatus, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel1MouseReleased(evt);
            }
        });

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/baron1.jpg")));
        jLabel4.setText("Brutium");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel1.add(jLabel4);

        jLabel5.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel5.setText("Ciudad: Crot\u00f3n");
        panel1.add(jLabel5);

        panel1.setBounds(20, 120, 110, 100);
        escritorio.add(panel1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel2MouseReleased(evt);
            }
        });

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/baron2.jpg")));
        jLabel6.setText("Apulia");
        jLabel6.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel2.add(jLabel6);

        jLabel7.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel7.setText("Ciudad: Tarentum");
        panel2.add(jLabel7);

        panel2.setBounds(20, 220, 110, 100);
        escritorio.add(panel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel4MouseReleased(evt);
            }
        });

        jLabel12.setBackground(new java.awt.Color(255, 255, 255));
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/coast1.jpg")));
        jLabel12.setText("Umbria");
        jLabel12.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel12.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel4.add(jLabel12);

        jLabel13.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel13.setText("Ciudad: Ariminun");
        panel4.add(jLabel13);

        panel4.setBounds(20, 520, 110, 100);
        escritorio.add(panel4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel_carta1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/evil_frame 30.jpg")));
        panel_carta1.add(jLabel20);

        panel_carta1.setBounds(20, 420, 110, 100);
        escritorio.add(panel_carta1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel3.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel3MouseReleased(evt);
            }
        });

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/baron3.jpg")));
        jLabel8.setText("Aetolia");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel3.add(jLabel8);

        jLabel9.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel9.setText("Ciudad: Thermon");
        panel3.add(jLabel9);

        panel3.setBounds(20, 320, 110, 100);
        escritorio.add(panel3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel5MouseReleased(evt);
            }
        });

        jLabel14.setBackground(new java.awt.Color(255, 255, 255));
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/coast2.jpg")));
        jLabel14.setText("Etruria");
        jLabel14.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel14.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel5.add(jLabel14);

        jLabel15.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel15.setText("Ciudad: Arretium");
        panel5.add(jLabel15);

        panel5.setBounds(20, 620, 110, 100);
        escritorio.add(panel5, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel6.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel6MouseReleased(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel17.setText("Ciudad: Patavium");
        panel6.add(jLabel17);

        jLabel16.setBackground(new java.awt.Color(255, 255, 255));
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/cumbria.jpg")));
        jLabel16.setText("Venetia");
        jLabel16.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel16.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel6.add(jLabel16);

        panel6.setBounds(130, 620, 160, 100);
        escritorio.add(panel6, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel7.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel7MouseReleased(evt);
            }
        });

        jLabel18.setBackground(new java.awt.Color(255, 255, 255));
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/coast3.jpg")));
        jLabel18.setText("Liguria");
        jLabel18.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel18.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel7.add(jLabel18);

        jLabel19.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel19.setText("Ciudad: Segesta");
        panel7.add(jLabel19);

        panel7.setBounds(290, 620, 110, 100);
        escritorio.add(panel7, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel8.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel8MouseReleased(evt);
            }
        });

        jLabel10.setBackground(new java.awt.Color(255, 255, 255));
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/black_forest.jpg")));
        jLabel10.setText("Epirus");
        jLabel10.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel10.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel8.add(jLabel10);

        jLabel11.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel11.setText("Ciudad: Apolonia");
        panel8.add(jLabel11);

        panel8.setBounds(290, 520, 110, 100);
        escritorio.add(panel8, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel9.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel9MouseReleased(evt);
            }
        });

        jLabel22.setBackground(new java.awt.Color(255, 255, 255));
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/forest1.jpg")));
        jLabel22.setText("Aquitania");
        jLabel22.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel22.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel9.add(jLabel22);

        jLabel23.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel23.setText("Ciudad: Lamonun");
        panel9.add(jLabel23);

        panel9.setBounds(290, 420, 110, 100);
        escritorio.add(panel9, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel_al_coliseo.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/gladiador.jpg")));
        panel_al_coliseo.add(jLabel21);

        panel_al_coliseo.setBounds(290, 320, 110, 100);
        escritorio.add(panel_al_coliseo, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel10.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel10MouseReleased(evt);
            }
        });

        jLabel26.setBackground(new java.awt.Color(255, 255, 255));
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/forest2.jpg")));
        jLabel26.setText("Lugdinensis");
        jLabel26.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel26.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel10.add(jLabel26);

        jLabel27.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel27.setText("Ciudad: Lugdumun");
        panel10.add(jLabel27);

        panel10.setBounds(180, 290, 110, 130);
        escritorio.add(panel10, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel11.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel11MouseReleased(evt);
            }
        });

        jLabel28.setBackground(new java.awt.Color(255, 255, 255));
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/forest3.jpg")));
        jLabel28.setText("Narbonensis");
        jLabel28.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel28.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel11.add(jLabel28);

        jLabel29.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel29.setText("Ciudad: Narbo");
        panel11.add(jLabel29);

        panel11.setBounds(180, 160, 110, 130);
        escritorio.add(panel11, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel12.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel12MouseReleased(evt);
            }
        });

        jLabel30.setBackground(new java.awt.Color(255, 255, 255));
        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/grass1.jpg")));
        jLabel30.setText("Taraconesis");
        jLabel30.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel30.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel12.add(jLabel30);

        jLabel31.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel31.setText("Ciudad: Osca");
        panel12.add(jLabel31);

        panel12.setBounds(290, 160, 110, 100);
        escritorio.add(panel12, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel13.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel13MouseReleased(evt);
            }
        });

        jLabel32.setBackground(new java.awt.Color(255, 255, 255));
        jLabel32.setForeground(new java.awt.Color(255, 255, 255));
        jLabel32.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/grass2.jpg")));
        jLabel32.setText("Celtiberia");
        jLabel32.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel32.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel13.add(jLabel32);

        jLabel33.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel33.setText("Ciudad: Numantia");
        panel13.add(jLabel33);

        panel13.setBounds(400, 160, 120, 100);
        escritorio.add(panel13, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel14.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel14MouseReleased(evt);
            }
        });

        jLabel34.setBackground(new java.awt.Color(255, 255, 255));
        jLabel34.setForeground(new java.awt.Color(255, 255, 255));
        jLabel34.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/grass3.jpg")));
        jLabel34.setText("Peloponesus");
        jLabel34.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel34.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel14.add(jLabel34);

        jLabel35.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel35.setText("Ciudad: Corinto");
        panel14.add(jLabel35);

        panel14.setBounds(520, 160, 110, 100);
        escritorio.add(panel14, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel15.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel15MouseReleased(evt);
            }
        });

        jLabel36.setBackground(new java.awt.Color(255, 255, 255));
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/mountain1.jpg")));
        jLabel36.setText("Aquitania");
        jLabel36.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel36.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel15.add(jLabel36);

        jLabel37.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel37.setText("Ciudad: Lemonum");
        panel15.add(jLabel37);

        panel15.setBounds(630, 160, 110, 130);
        escritorio.add(panel15, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel16.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel16MouseReleased(evt);
            }
        });

        jLabel38.setBackground(new java.awt.Color(255, 255, 255));
        jLabel38.setForeground(new java.awt.Color(255, 255, 255));
        jLabel38.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/mountain2.jpg")));
        jLabel38.setText("Armorica");
        jLabel38.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel38.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel16.add(jLabel38);

        jLabel39.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel39.setText("Ciudad: Redonum");
        panel16.add(jLabel39);

        panel16.setBounds(630, 290, 110, 130);
        escritorio.add(panel16, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel_ba�os.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/banos publicos 2.jpg")));
        panel_ba�os.add(jLabel24);

        panel_ba�os.setBounds(520, 320, 110, 100);
        escritorio.add(panel_ba�os, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel17.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel17MouseReleased(evt);
            }
        });

        jLabel42.setBackground(new java.awt.Color(255, 255, 255));
        jLabel42.setForeground(new java.awt.Color(255, 255, 255));
        jLabel42.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/mountain3.jpg")));
        jLabel42.setText("B\u00e9lgica");
        jLabel42.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel42.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel17.add(jLabel42);

        jLabel43.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel43.setText("Ciudad: Samarobriva");
        panel17.add(jLabel43);

        panel17.setBounds(520, 420, 110, 100);
        escritorio.add(panel17, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel18.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel18MouseReleased(evt);
            }
        });

        jLabel44.setBackground(new java.awt.Color(255, 255, 255));
        jLabel44.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/sahara.jpg")));
        jLabel44.setText("Cicilia");
        jLabel44.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel44.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel18.add(jLabel44);

        jLabel45.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel45.setText("Ciudad: Tarsus");
        panel18.add(jLabel45);

        panel18.setBounds(520, 520, 110, 100);
        escritorio.add(panel18, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel19.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel19MouseReleased(evt);
            }
        });

        jLabel46.setBackground(new java.awt.Color(255, 255, 255));
        jLabel46.setForeground(new java.awt.Color(255, 255, 255));
        jLabel46.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/sand1.jpg")));
        jLabel46.setText("Assyria");
        jLabel46.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel46.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel19.add(jLabel46);

        jLabel47.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel47.setText("Ciudad: Hatra");
        panel19.add(jLabel47);

        panel19.setBounds(520, 620, 110, 100);
        escritorio.add(panel19, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel_coliseum.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/coliseo.jpg")));
        panel_coliseum.add(jLabel25);

        panel_coliseum.setBounds(790, 620, 110, 100);
        escritorio.add(panel_coliseum, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel20.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel20MouseReleased(evt);
            }
        });

        jLabel49.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel49.setText("Ciudad: Cartago");
        panel20.add(jLabel49);

        jLabel48.setBackground(new java.awt.Color(255, 255, 255));
        jLabel48.setForeground(new java.awt.Color(255, 255, 255));
        jLabel48.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/sand2.jpg")));
        jLabel48.setText("Africa");
        jLabel48.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel48.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel20.add(jLabel48);

        panel20.setBounds(630, 620, 160, 100);
        escritorio.add(panel20, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel21.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel21MouseReleased(evt);
            }
        });

        jLabel52.setBackground(new java.awt.Color(255, 255, 255));
        jLabel52.setForeground(new java.awt.Color(255, 255, 255));
        jLabel52.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/sand3.jpg")));
        jLabel52.setText("Judaea");
        jLabel52.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel52.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel21.add(jLabel52);

        jLabel53.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel53.setText("Ciudad: Jerusalem");
        panel21.add(jLabel53);

        panel21.setBounds(790, 520, 110, 100);
        escritorio.add(panel21, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel22.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel22.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel22MouseReleased(evt);
            }
        });

        jLabel54.setBackground(new java.awt.Color(255, 255, 255));
        jLabel54.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/sea.jpg")));
        jLabel54.setText("Boihaemun");
        jLabel54.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel54.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel22.add(jLabel54);

        jLabel55.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel55.setText("Ciudad: Lovosice");
        panel22.add(jLabel55);

        panel22.setBounds(790, 420, 110, 100);
        escritorio.add(panel22, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel23.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel23MouseReleased(evt);
            }
        });

        jLabel57.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel57.setText("Ciudad: Trier");
        panel23.add(jLabel57);

        jLabel56.setBackground(new java.awt.Color(255, 255, 255));
        jLabel56.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/SKY_BK.jpg")));
        jLabel56.setText("Germania Supra");
        jLabel56.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel56.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel23.add(jLabel56);

        panel23.setBounds(790, 320, 110, 100);
        escritorio.add(panel23, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel_carta2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        jLabel40.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/evil_frame 30.jpg")));
        panel_carta2.add(jLabel40);

        panel_carta2.setBounds(790, 220, 110, 100);
        escritorio.add(panel_carta2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel26.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel26.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel26MouseReleased(evt);
            }
        });

        jLabel65.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel65.setText("Ciudad: Venedae");
        panel26.add(jLabel65);

        jLabel64.setBackground(new java.awt.Color(255, 255, 255));
        jLabel64.setForeground(new java.awt.Color(255, 255, 255));
        jLabel64.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/SKY_BK_3.jpg")));
        jLabel64.setText("Pripet");
        jLabel64.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel64.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel26.add(jLabel64);

        panel26.setBounds(680, 20, 110, 100);
        escritorio.add(panel26, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel25.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel25.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel25MouseReleased(evt);
            }
        });

        jLabel63.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel63.setText("Ciudad: Vicus Gothi");
        panel25.add(jLabel63);

        jLabel62.setBackground(new java.awt.Color(255, 255, 255));
        jLabel62.setForeground(new java.awt.Color(255, 255, 255));
        jLabel62.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/SKY_BK_2.jpg")));
        jLabel62.setText("Locus Gothi");
        jLabel62.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel62.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel25.add(jLabel62);

        panel25.setBounds(790, 20, 110, 100);
        escritorio.add(panel25, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel24.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel24.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel24MouseReleased(evt);
            }
        });

        jLabel61.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel61.setText("Ciudad: Bordesholm");
        panel24.add(jLabel61);

        jLabel60.setBackground(new java.awt.Color(255, 255, 255));
        jLabel60.setForeground(new java.awt.Color(255, 255, 255));
        jLabel60.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/SKY_BK_10.jpg")));
        jLabel60.setText("Saxones");
        jLabel60.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel60.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel24.add(jLabel60);

        panel24.setBounds(790, 120, 110, 100);
        escritorio.add(panel24, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel27.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel27.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel27MouseReleased(evt);
            }
        });

        jLabel67.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel67.setText("Ciudad: Porrolissum");
        panel27.add(jLabel67);

        jLabel66.setBackground(new java.awt.Color(255, 255, 255));
        jLabel66.setForeground(new java.awt.Color(255, 255, 255));
        jLabel66.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/SKY_BK_4.jpg")));
        jLabel66.setText("Dacia");
        jLabel66.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel66.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel27.add(jLabel66);

        panel27.setBounds(570, 20, 110, 100);
        escritorio.add(panel27, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel28.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel28MouseReleased(evt);
            }
        });

        jLabel69.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel69.setText("Ciudad: Tylis");
        panel28.add(jLabel69);

        jLabel68.setBackground(new java.awt.Color(255, 255, 255));
        jLabel68.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/SKY_BK_6.jpg")));
        jLabel68.setText("Thrace");
        jLabel68.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel68.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel28.add(jLabel68);

        panel28.setBounds(460, 20, 110, 100);
        escritorio.add(panel28, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel29.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel29MouseReleased(evt);
            }
        });

        jLabel71.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel71.setText("Ciudad: Byzantium");
        panel29.add(jLabel71);

        jLabel70.setBackground(new java.awt.Color(255, 255, 255));
        jLabel70.setForeground(new java.awt.Color(255, 255, 255));
        jLabel70.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/SKY_BK_7.jpg")));
        jLabel70.setText("Propontis");
        jLabel70.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel70.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel29.add(jLabel70);

        panel29.setBounds(350, 20, 110, 100);
        escritorio.add(panel29, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel30.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel30MouseReleased(evt);
            }
        });

        jLabel73.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel73.setText("Ciudad: Atenas");
        panel30.add(jLabel73);

        jLabel72.setBackground(new java.awt.Color(255, 255, 255));
        jLabel72.setForeground(new java.awt.Color(255, 255, 255));
        jLabel72.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/SKY_BK_8.jpg")));
        jLabel72.setText("\u00cdtaca");
        jLabel72.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel72.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel30.add(jLabel72);

        panel30.setBounds(240, 20, 110, 100);
        escritorio.add(panel30, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/Flecha.JPG")));
        jLabel2.setBounds(130, 120, -1, -1);
        escritorio.add(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel31.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        panel31.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                panel31MouseReleased(evt);
            }
        });

        jLabel75.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
        jLabel75.setText("Ciudad: C\u00f3rdoba");
        panel31.add(jLabel75);

        jLabel74.setBackground(new java.awt.Color(255, 255, 255));
        jLabel74.setForeground(new java.awt.Color(255, 255, 255));
        jLabel74.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/SKY_BK_9.jpg")));
        jLabel74.setText("Ba\u00e9tica");
        jLabel74.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel74.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        panel31.add(jLabel74);

        panel31.setBounds(130, 20, 110, 100);
        escritorio.add(panel31, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel_cara_jugador.setBackground(new java.awt.Color(0, 0, 0));
        panel_cara_jugador.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
        label_jugador.setForeground(new java.awt.Color(255, 255, 255));
        label_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_brutii.jpg")));
        label_jugador.setText("Jugador 1");
        label_jugador.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        label_jugador.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        label_jugador.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                label_jugadorMouseReleased(evt);
            }
        });

        panel_cara_jugador.add(label_jugador);

        panel_cara_jugador.setBounds(150, 440, 120, 160);
        escritorio.add(panel_cara_jugador, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jPanel40.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel40.setBackground(new java.awt.Color(0, 0, 0));
        jPanel40.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true));
        jLabel76.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/evil_frame 80.jpg")));
        jPanel40.add(jLabel76, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 160));

        jPanel40.setBounds(650, 440, 120, 160);
        escritorio.add(jPanel40, javax.swing.JLayeredPane.DEFAULT_LAYER);

        panel_opciones.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        panel_opciones.setBackground(new java.awt.Color(255, 255, 255));
        panel_opciones.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));
        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });

        panel_opciones.add(btn_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 660, 90, 70));

        panel_opciones.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 110, -1));

        btn_tirar_dados.setFont(new java.awt.Font("Tahoma", 0, 10));
        btn_tirar_dados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/dados.JPG")));
        btn_tirar_dados.setMnemonic('t');
        btn_tirar_dados.setText("Tirar  Dados");
        btn_tirar_dados.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_tirar_dados.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btn_tirar_dados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tirar_dadosActionPerformed(evt);
            }
        });

        panel_opciones.add(btn_tirar_dados, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 90, 60));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setOpaque(false);
        panel_opciones.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 30, -1, -1));

        dado_uno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/uno.JPG")));
        panel_opciones.add(dado_uno, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, -1));

        dado_dos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/uno negro.JPG")));
        panel_opciones.add(dado_dos, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 90, -1, -1));

        btn_pelear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/RomanHelm.JPG")));
        btn_pelear.setMnemonic('p');
        btn_pelear.setText("Pelear");
        btn_pelear.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_pelear.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btn_pelear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_pelearActionPerformed(evt);
            }
        });

        panel_opciones.add(btn_pelear, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 90, 60));

        btn_fin_turno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_senate.jpg")));
        btn_fin_turno.setMnemonic('f');
        btn_fin_turno.setText("Fin");
        btn_fin_turno.setToolTipText("Termina el turno del jugador actual");
        btn_fin_turno.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_fin_turno.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btn_fin_turno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_fin_turnoActionPerformed(evt);
            }
        });

        panel_opciones.add(btn_fin_turno, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 310, 90, -1));

        btn_opciones.setMnemonic('o');
        btn_opciones.setText("Opciones");
        btn_opciones.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_opciones.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btn_opciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_opcionesActionPerformed(evt);
            }
        });

        panel_opciones.add(btn_opciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 90, 90));

        jLabel41.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/marmol.JPG")));
        panel_opciones.add(jLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(-20, 0, -1, -1));

        panel_opciones.setBounds(910, 0, 110, 740);
        escritorio.add(panel_opciones, javax.swing.JLayeredPane.DEFAULT_LAYER);

        getContentPane().add(escritorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(1, 1, 1020, 740));

        pack();
    }
    // </editor-fold>//GEN-END:initComponents
    
    private void btn_opcionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_opcionesActionPerformed
        mostrarVentanaJugador();
    }//GEN-LAST:event_btn_opcionesActionPerformed
    
    // <editor-fold defaultstate="collapsed" desc=" M�todos para mostrar las ventanas de cada regi�n ">
    private void panel31MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel31MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel31MouseReleased
    
    private void panel30MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel30MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel30MouseReleased
    
    private void panel29MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel29MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel29MouseReleased
    
    private void panel28MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel28MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel28MouseReleased
    
    private void panel27MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel27MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel27MouseReleased
    
    private void panel26MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel26MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel26MouseReleased
    
    private void panel25MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel25MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel25MouseReleased
    
    private void panel24MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel24MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel24MouseReleased
    
    private void panel23MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel23MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel23MouseReleased
    
    private void panel22MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel22MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel22MouseReleased
    
    private void panel21MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel21MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel21MouseReleased
    
    private void panel20MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel20MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel20MouseReleased
    
    private void panel19MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel19MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel19MouseReleased
    
    private void panel18MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel18MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel18MouseReleased
    
    private void panel17MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel17MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel17MouseReleased
    
    private void panel16MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel16MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel16MouseReleased
    
    private void panel15MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel15MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel15MouseReleased
    
    private void panel14MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel14MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel14MouseReleased
    
    private void panel13MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel13MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel13MouseReleased
    
    private void panel12MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel12MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel12MouseReleased
    
    private void panel11MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel11MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel11MouseReleased
    
    private void panel10MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel10MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel10MouseReleased
    
    private void panel9MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel9MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel9MouseReleased
    
    private void panel8MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel8MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel8MouseReleased
    
    private void panel7MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel7MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel7MouseReleased
    
    private void panel6MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel6MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel6MouseReleased
    
    private void panel5MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel5MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel5MouseReleased
    
    private void panel4MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel4MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel4MouseReleased
    
    private void panel3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel3MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel3MouseReleased
    
    private void panel2MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel2MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel2MouseReleased
    //</editor-fold>
    
    private void btn_fin_turnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_fin_turnoActionPerformed
        finDelTurno();
    }//GEN-LAST:event_btn_fin_turnoActionPerformed
    
    private void panel1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panel1MouseReleased
        mostrarRegion( evt );
    }//GEN-LAST:event_panel1MouseReleased
    
    private void btn_pelearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_pelearActionPerformed
        pintarCasilla();
        Region r = null;
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase("Brutii") ){
            if( Mesa.calcularRegion(Mesa.getPos_brutii()) instanceof Region ){
                r = Mesa.calcularRegion(Mesa.getPos_brutii());
            }   //fin del if
        }   //fin del if
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase("Scipii") ){
            if( Mesa.calcularRegion(Mesa.getPos_scipii()) instanceof Region ){
                r = Mesa.calcularRegion(Mesa.getPos_scipii());
            }   //fin del if
        }   //fin del if
        if( Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase("Julii") ){
            if( Mesa.calcularRegion(Mesa.getPos_julii()) instanceof Region ){
                r = Mesa.calcularRegion(Mesa.getPos_julii());
            }   //fin del if
        }   //fin del if
        mostrarVentanaGuerra( Mesa.jugadorActual().getEmperador(), r );
        
    }//GEN-LAST:event_btn_pelearActionPerformed
    
    private void label_jugadorMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_label_jugadorMouseReleased
        if (evt.getButton() == evt.BUTTON3){
            mostrarVentanaJugador();
        }   //fin del if
    }//GEN-LAST:event_label_jugadorMouseReleased
    
    // <editor-fold defaultstate="collapsed" desc=" btn_tirar_dadosActionPerformed ">
    private void btn_tirar_dadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tirar_dadosActionPerformed
        pintarCasilla();
        int dado1 = Mesa.tirarDados();
        switch( dado1 ){
            case 1: dado_uno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/uno.JPG")));
            break;
            case 2: dado_uno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/dos.JPG")));
            break;
            case 3: dado_uno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tres.JPG")));
            break;
            case 4: dado_uno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cuatro.JPG")));
            break;
            case 5: dado_uno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cinco.JPG")));
            break;
            case 6: dado_uno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/seis.JPG")));
            break;
        }   //fin del switch
        
        int dado2 = Mesa.tirarDados();
        switch( dado2 ){
            case 1: dado_dos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/uno negro.JPG")));
            break;
            case 2: dado_dos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/dos negro.JPG")));
            break;
            case 3: dado_dos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tres negro.JPG")));
            break;
            case 4: dado_dos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cuatro negro.JPG")));
            break;
            case 5: dado_dos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cinco negro.JPG")));
            break;
            case 6: dado_dos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/seis negro.JPG")));
            break;
        }   //fin del switch
        if( dado1 != dado2 ){
            btn_tirar_dados.setEnabled(false);
            actualizarBoton();
        }else{
            JOptionPane.showMessageDialog( null, "�Felicidades! El Senatvs ha valorado tus esfuerzos y te ha " +
                    "\npermitido tirar una vez m�s los dados ", "Mandatvm", JOptionPane.INFORMATION_MESSAGE );
            Mesa.jugadorActual().getEmperador().setEncarcelado(false);
        }
        int valor = dado1 + dado2;
        Mesa.getJug().setDado1(dado1);
        Mesa.getJug().setDado2(dado2);
        Mesa.jugadorActual().getEmperador().validaEncarcelado();
        if( !Mesa.jugadorActual().getEmperador().isEncarcelado() ){
            moverFicha( valor, Mesa.jugadorActual().getEmperador().getFamilia() );
            actualizarBoton();
        }   //fin del if
    }//GEN-LAST:event_btn_tirar_dadosActionPerformed
//</editor-fold>
    
    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        System.exit( 0 );
    }//GEN-LAST:event_btn_salirActionPerformed
    
    // <editor-fold defaultstate="collapsed" desc=" declaracion de variables ">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_fin_turno;
    private javax.swing.JButton btn_opciones;
    private javax.swing.JButton btn_pelear;
    private javax.swing.JButton btn_salir;
    private javax.swing.JButton btn_tirar_dados;
    private javax.swing.JLabel dado_dos;
    private javax.swing.JLabel dado_uno;
    private javax.swing.JDesktopPane escritorio;
    private javax.swing.JLabel ficha_brutii;
    private javax.swing.JLabel ficha_julii;
    private javax.swing.JLabel ficha_scipii;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel40;
    private javax.swing.JLabel label_jugador;
    private javax.swing.JPanel panel1;
    private javax.swing.JPanel panel10;
    private javax.swing.JPanel panel11;
    private javax.swing.JPanel panel12;
    private javax.swing.JPanel panel13;
    private javax.swing.JPanel panel14;
    private javax.swing.JPanel panel15;
    private javax.swing.JPanel panel16;
    private javax.swing.JPanel panel17;
    private javax.swing.JPanel panel18;
    private javax.swing.JPanel panel19;
    private javax.swing.JPanel panel2;
    private javax.swing.JPanel panel20;
    private javax.swing.JPanel panel21;
    private javax.swing.JPanel panel22;
    private javax.swing.JPanel panel23;
    private javax.swing.JPanel panel24;
    private javax.swing.JPanel panel25;
    private javax.swing.JPanel panel26;
    private javax.swing.JPanel panel27;
    private javax.swing.JPanel panel28;
    private javax.swing.JPanel panel29;
    private javax.swing.JPanel panel3;
    private javax.swing.JPanel panel30;
    private javax.swing.JPanel panel31;
    private javax.swing.JPanel panel4;
    private javax.swing.JPanel panel5;
    private javax.swing.JPanel panel6;
    private javax.swing.JPanel panel7;
    private javax.swing.JPanel panel8;
    private javax.swing.JPanel panel9;
    private javax.swing.JPanel panel_al_coliseo;
    private javax.swing.JPanel panel_ba�os;
    private javax.swing.JPanel panel_cara_jugador;
    private javax.swing.JPanel panel_carta1;
    private javax.swing.JPanel panel_carta2;
    private javax.swing.JPanel panel_coliseum;
    private javax.swing.JPanel panel_opciones;
    private javax.swing.JPanel panel_senatus;
    // End of variables declaration//GEN-END:variables
    private final Color VERDE = new Color(50, 100, 50 );
    private final Color ROJO = new Color( 150, 0, 0 );
    private final Color AZUL = new Color( 50, 50, 100 );
    private final Color VERDE_CASILLA = new Color(99, 204, 00 );
    private final Color ROJO_CASILLA = new Color( 255, 111, 72 );
    private final Color AZUL_CASILLA = new Color( 33, 99, 255 );
    private int numero_turno_brutii;
    private int numero_turno_scipii;
    private int numero_turno_julii;
    private Icon iconoCarta;
    // </editor-fold>
    
}   //fin de la clase VentanaMesa