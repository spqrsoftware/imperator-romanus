package guis;

import codigo.*;

public class VentanaGuerra extends javax.swing.JInternalFrame {
    
    /** Creates new form VentanaGuerra */
    public VentanaGuerra( Imperator imp, Region r ) {
        initComponents();
        this.imp = imp;
        this.r  = r;
        actualizarVentana(imp, r );
    }
    
    public void actualizarVentana( Imperator imp, Region r ){
        if( r.getEmperador() == null ){
            label_region1.setText( r.getNombre() );
            label_ciudad1.setText( r.getCiudad() );
            label_emperador.setText( imp.getNombre() );
            label_ejercito_imp.setText( String.valueOf( imp.getEjercito().size() ));
            if( imp.getFamilia().equalsIgnoreCase( "Brutii" )){
                label_emperador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_brutii.jpg")) );
            }   //fin del if
            if( imp.getFamilia().equalsIgnoreCase( "Scipii" )){
                label_emperador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_scipii.jpg")) );
            }   //fin del if
            if( imp.getFamilia().equalsIgnoreCase( "Julii" )){
                label_emperador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_julii.jpg")) );
            }   //fin del if
        }else{
            label_emperador.setText( imp.getNombre() );
            label_ejercito_imp.setText( String.valueOf( imp.getEjercito().size() ));
            label_region1.setText( r.getNombre() );
            label_ciudad1.setText( r.getCiudad() );
            label_ejercito_region1.setText( String.valueOf( r.getEjercito().size() ));
            if( imp.getFamilia().equalsIgnoreCase( "Brutii" )){
                label_emperador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_brutii.jpg")) );
            }   //fin del if
            if( imp.getFamilia().equalsIgnoreCase( "Scipii" )){
                label_emperador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_scipii.jpg")) );
            }   //fin del if
            if( imp.getFamilia().equalsIgnoreCase( "Julii" )){
                label_emperador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_julii.jpg")) );
            }   //fin del if
            if( r.getEmperador().getFamilia().equalsIgnoreCase( "Brutii" )){
                label_imp_region1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_brutii.jpg")) );
                label_imp_region1.setText( r.getEmperador().getNombre() );
            }   //fin del if
            if( r.getEmperador().getFamilia().equalsIgnoreCase( "Scipii" )){
                label_imp_region1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_scipii.jpg")) );
                label_imp_region1.setText( r.getEmperador().getNombre() );
            }   //fin del if
            if( r.getEmperador().getFamilia().equalsIgnoreCase( "Julii" )){
                label_imp_region1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/leader_pic_julii.jpg")) );
                label_imp_region1.setText( r.getEmperador().getNombre() );
            }   //fin del if
        }   //fin del if...else
        if( imp.equals(r.getEmperador() )){
            btn_pelear.setEnabled( false );
        }else{
            btn_pelear.setEnabled( true );
        }   //fin del if...else
    }   //fin del m�todo actualizarVentana
    
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jLabel1 = new javax.swing.JLabel();
        btn_pelear = new javax.swing.JButton();
        label_emperador = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        label_ejercito_imp = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        label_ejercito_region1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        label_region1 = new javax.swing.JLabel();
        label_ciudad1 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        label_imp_region1 = new javax.swing.JLabel();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        setClosable(true);
        setTitle("\u00a1A Batalla!");
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/regiones/provincial_campaign.jpg")));
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 0, -1, -1));

        btn_pelear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/RomanHelm.JPG")));
        btn_pelear.setMnemonic('p');
        btn_pelear.setText("\u00a1Pelear!");
        btn_pelear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_pelearActionPerformed(evt);
            }
        });

        getContentPane().add(btn_pelear, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 300, 140, 40));

        label_emperador.setFont(new java.awt.Font("MS Sans Serif", 1, 24));
        label_emperador.setText("Emperador");
        label_emperador.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        label_emperador.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        getContentPane().add(label_emperador, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, -1));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("MS Sans Serif", 1, 12));
        jLabel2.setText("Ej\u00e9rcito:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 310, -1, -1));

        label_ejercito_imp.setFont(new java.awt.Font("MS Sans Serif", 1, 24));
        label_ejercito_imp.setText("0");
        jPanel1.add(label_ejercito_imp, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 290, -1, 50));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 210, 340));

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jPanel2.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 10, 210));

        label_ejercito_region1.setFont(new java.awt.Font("MS Sans Serif", 1, 24));
        label_ejercito_region1.setText("0");
        jPanel2.add(label_ejercito_region1, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 290, -1, 50));

        jLabel5.setFont(new java.awt.Font("MS Sans Serif", 1, 12));
        jLabel5.setText("Ej\u00e9rcito:");
        jPanel2.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 310, -1, -1));

        label_region1.setFont(new java.awt.Font("MS Sans Serif", 1, 18));
        label_region1.setText("Regi\u00f3n");
        jPanel2.add(label_region1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, -1, -1));

        label_ciudad1.setFont(new java.awt.Font("MS Sans Serif", 1, 16));
        label_ciudad1.setText("Ciudad");
        jPanel2.add(label_ciudad1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, -1, -1));

        jLabel6.setText("Pertenece a:");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, -1, -1));

        label_imp_region1.setFont(new java.awt.Font("MS Sans Serif", 0, 12));
        label_imp_region1.setText("B\u00e1rbaros");
        label_imp_region1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        label_imp_region1.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel2.add(label_imp_region1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 159, 100, 120));

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 0, 200, 340));

        pack();
    }
    // </editor-fold>//GEN-END:initComponents
    
    private void btn_pelearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_pelearActionPerformed
        Mesa.conquistarRegion( imp, r );
        actualizarVentana(imp, r );
        Mesa.getJug().setPeleo(true);
    }//GEN-LAST:event_btn_pelearActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_pelear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel label_ciudad1;
    private javax.swing.JLabel label_ejercito_imp;
    private javax.swing.JLabel label_ejercito_region1;
    private javax.swing.JLabel label_emperador;
    private javax.swing.JLabel label_imp_region1;
    private javax.swing.JLabel label_region1;
    // End of variables declaration//GEN-END:variables
    Imperator imp = null;
    Region r = null;
}