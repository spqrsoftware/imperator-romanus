
package guis;

import codigo.*;
import estructuras.ListaOrdenada;

/**
 *
 * @author  __USER__
 */
public class VentanaConsulta extends javax.swing.JFrame {
    
    /** Creates new form VentanaConsulta */
    public VentanaConsulta() {
        initComponents();
        jScrollPane1.setVisible(false);
    }
    
    private void desplegarJugadas(String familia){
        ListaOrdenada lista = Mesa.getVitacora(familia);
        boolean seguir = true;
        for (int i = 0; (i < 3) && seguir; i++){
            if (Mesa.jugadorActual().getEmperador().getFamilia().equalsIgnoreCase(familia)){
                seguir = false;
                String msn = Mesa.jugadorActual().toString();
                msn += "\n\n" + lista.visualizar();
                area.setText(msn);
            }else{
                Jugador jug = Mesa.jugadorSiguiente();
            }
        }
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() {
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        btn_salir = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        area = new javax.swing.JTextArea();
        btn_Brutii = new javax.swing.JToggleButton();
        btn_Julii = new javax.swing.JToggleButton();
        btn_Scipii = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Vitacora del juego");
        setUndecorated(true);
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 0, -1, -1));

        jLabel3.setFont(new java.awt.Font("Californian FB", 1, 72));
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/Imperator Romanu logo.jpg")));
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 10, -1, -1));

        jLabel22.setFont(new java.awt.Font("Californian FB", 1, 14));
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("Seleccione uno");
        getContentPane().add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 180, 130, -1));

        btn_salir.setFont(new java.awt.Font("MS Sans Serif", 1, 15));
        btn_salir.setMnemonic('q');
        btn_salir.setText("Salir");
        btn_salir.setToolTipText("Sale del juego");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });

        getContentPane().add(btn_salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 10, 140, 60));

        area.setEditable(false);
        area.setFont(new java.awt.Font("Arial", 0, 14));
        jScrollPane1.setViewportView(area);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 350, 860, 350));

        btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_grey.jpg")));
        btn_Brutii.setMnemonic('B');
        btn_Brutii.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_BrutiiActionPerformed(evt);
            }
        });
        btn_Brutii.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_BrutiiMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_BrutiiMouseExited(evt);
            }
        });

        getContentPane().add(btn_Brutii, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 210, 60, 50));

        btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_grey.jpg")));
        btn_Julii.setMnemonic('J');
        btn_Julii.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_JuliiActionPerformed(evt);
            }
        });
        btn_Julii.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_JuliiMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_JuliiMouseExited(evt);
            }
        });

        getContentPane().add(btn_Julii, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 210, 60, 50));

        btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_grey.jpg")));
        btn_Scipii.setMnemonic('S');
        btn_Scipii.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ScipiiActionPerformed(evt);
            }
        });
        btn_Scipii.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btn_ScipiiMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btn_ScipiiMouseExited(evt);
            }
        });

        getContentPane().add(btn_Scipii, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 210, 60, 50));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/loading_screen_16.jpg")));
        jLabel1.setOpaque(true);
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }
    // </editor-fold>//GEN-END:initComponents
    
    // <editor-fold defaultstate="collapsed" desc=" boton Brutii ">
    private void btn_BrutiiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_BrutiiMouseExited
        if( btn_Brutii.isSelected() ){
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_roll.jpg")));
        }else{
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_grey.jpg")));
        }   //fin del if...else
    }//GEN-LAST:event_btn_BrutiiMouseExited
    
    private void btn_BrutiiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_BrutiiMouseEntered
        if( btn_Brutii.isSelected() ){
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_roll.jpg")));
        }else{
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii.jpg")));
        }   //fin del if...else
    }//GEN-LAST:event_btn_BrutiiMouseEntered
    
    private void btn_BrutiiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_BrutiiActionPerformed
        if( btn_Brutii.isSelected() ){
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_roll.jpg")));
            btn_Scipii.setSelected(false);
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_grey.jpg")));
            btn_Julii.setSelected(false);
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_grey.jpg")));
            jScrollPane1.setVisible(true);
            area.setText("No participo del juego");
            desplegarJugadas("brutii");
        }else{
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_grey.jpg")));
            area.setText("");
            jScrollPane1.setVisible(false);
        }   //fin del if...else
    }//GEN-LAST:event_btn_BrutiiActionPerformed
    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" botron Julii ">
    private void btn_JuliiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_JuliiMouseExited
        if( btn_Julii.isSelected() ){
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_roll.jpg")));
        }else{
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_grey.jpg")));
        }   //fin del if...else
    }//GEN-LAST:event_btn_JuliiMouseExited
    
    private void btn_JuliiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_JuliiMouseEntered
        if( btn_Julii.isSelected() ){
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_roll.jpg")));
        }else{
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii.jpg")));
        }   //fin del if...else
    }//GEN-LAST:event_btn_JuliiMouseEntered
    
    private void btn_JuliiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_JuliiActionPerformed
        if( btn_Julii.isSelected() ){
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_roll.jpg")));
            btn_Scipii.setSelected(false);
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_grey.jpg")));
            btn_Brutii.setSelected(false);
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_grey.jpg")));
            jScrollPane1.setVisible(true);
            area.setText("No participo del juego");
            desplegarJugadas("julii");
        }else{
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_grey.jpg")));
            area.setText("");
            jScrollPane1.setVisible(false);
        }   //fin del if...else
    }//GEN-LAST:event_btn_JuliiActionPerformed
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" boton Scipii ">
    private void btn_ScipiiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_ScipiiMouseExited
        if( btn_Scipii.isSelected() ){
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_roll.jpg")));
        }else{
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_grey.jpg")));
        }   //fin del if...else
    }//GEN-LAST:event_btn_ScipiiMouseExited
    
    private void btn_ScipiiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btn_ScipiiMouseEntered
        if( btn_Scipii.isSelected() ){
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_roll.jpg")));
        }else{
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii.jpg")));
        }   //fin del if...else
    }//GEN-LAST:event_btn_ScipiiMouseEntered
    
    private void btn_ScipiiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ScipiiActionPerformed
        if( btn_Scipii.isSelected() ){
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_roll.jpg")));
            btn_Julii.setSelected(false);
            btn_Julii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_julii_grey.jpg")));
            btn_Brutii.setSelected(false);
            btn_Brutii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_brutii_grey.jpg")));
            jScrollPane1.setVisible(true);
            area.setText("No participo del juego");
            desplegarJugadas("scipii");
        }else{
            btn_Scipii.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/iconos/symbol48_scipii_grey.jpg")));
            area.setText("");
            jScrollPane1.setVisible(false);
        }   //fin del if...else
    }//GEN-LAST:event_btn_ScipiiActionPerformed
    //</editor-fold>
    
    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btn_salirActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea area;
    private javax.swing.JToggleButton btn_Brutii;
    private javax.swing.JToggleButton btn_Julii;
    private javax.swing.JToggleButton btn_Scipii;
    private javax.swing.JButton btn_salir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panel_brutii;
    private javax.swing.JPanel panel_brutii1;
    private javax.swing.JPanel panel_brutii2;
    private javax.swing.JPanel panel_julii;
    private javax.swing.JPanel panel_julii1;
    private javax.swing.JPanel panel_julii2;
    private javax.swing.JPanel panel_scipii;
    private javax.swing.JPanel panel_scipii1;
    private javax.swing.JPanel panel_scipii2;
    private javax.swing.JTextField txt_jugador1;
    private javax.swing.JTextField txt_jugador2;
    private javax.swing.JTextField txt_jugador3;
    private javax.swing.JTextField txt_jugador4;
    private javax.swing.JTextField txt_jugador5;
    private javax.swing.JTextField txt_jugador6;
    private javax.swing.JTextField txt_jugador7;
    private javax.swing.JTextField txt_jugador8;
    private javax.swing.JTextField txt_jugador9;
    // End of variables declaration//GEN-END:variables
    
}
